package model;

import java.util.Random;

public class Client {
    private int serviceTime;
    private int waitingTime;
    private int arrivalTime;
    int totalWaitingTime = 0;
    private String name;
    private int ID;
    private static int nrClients = 0;

    Client() {
        Random rnd = new Random();
        serviceTime = rnd.nextInt(Server.MAX_SERVICE_TIME) + Server.MIN_SERVICE_TIME;
        waitingTime = serviceTime;
        arrivalTime = rnd.nextInt(Server.MAX_ARRIVAL_TIME) + Server.MIN_ARRIVAL_TIME;
        name = Server.CLIENT_NAMES[rnd.nextInt(Server.CLIENT_NAMES.length)];
        nrClients++;
        ID = nrClients;
    }

    boolean decreaseTime() {
        waitingTime--;
        return waitingTime > 0;
    }

    double waitRate() { return (double)(serviceTime - waitingTime) / (double)totalWaitingTime; }
    double progressRate() { return (double) waitingTime / serviceTime; }

    int getArrivalTime() { return arrivalTime; }

    public String toString() { return name + "-" + ID; }
}
