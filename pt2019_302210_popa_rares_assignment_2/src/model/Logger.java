package model;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Logger {
    private static ArrayList<String> record = new ArrayList<>();
    private static TextArea textLog = new TextArea();

    static public void linkLog(TextArea t) {
        textLog = t;
        textLog.setText("SIMULATION START\n");
    }

    static synchronized void add(String s, int time) {
        record.add((time > 720 ? "OVERTIME" : "") + ">[T:" + time + "] " + s);
        Platform.runLater(() -> textLog.appendText((time > 720 ? "OVERTIME" : "") + ">[T:" + time + "] " + s + '\n'));
    }

    public static void saveLog(Server server) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("[yyyy-MM-dd]_[HH-mm-ss]");
            String fileName = "SIM-" + (dateFormat.format(new Date())) + ".txt";
            File file = new File(fileName);
            FileOutputStream fos = new FileOutputStream(fileName);

            fos.write("Simulation parameters: ".getBytes());
            fos.write(( "\n\tNumber of queues: " + server.getnrQueues() +
                        "\n\tNumber of clients: " + Server.CLIENT_NUMBER +
                        "\n\tQueue capacity: " + Server.MAX_QUEUE_CAPACITY +
                        "\n\tMinimum service time: " + Server.MIN_SERVICE_TIME +
                        "\n\tMaximum service time: " + Server.MAX_SERVICE_TIME +
                        "\n-------------------------------------------------------\n\n").getBytes());
            fos.write( "Simulation results: ".getBytes());
            fos.write(( "\n\tAverage speed: " + server.calculateAvgSpeed() +
                        "\n\tAverage waiting time: " + server.calculateAvgWait() +
                        "\n\tPeak time: " + server.getPeakTime() +
                        "\n-------------------------------------------------------\n\n").getBytes());
            fos.write( "Queue information: ".getBytes());
            for(Queue Q: server.getQueues()) {
                fos.write(( "\n\tQueue " + Q.getQueueNr() + ":" +
                            "\n\t\tClients processed: " + Q.getProcessed() +
                            "\n\t\tSpeed: " + Q.getSpeed() +
                            "\n\t\tAverage waiting time: " + Q.averageWait() +
                            "\n\t\tTime spent empty: " + Q.getEmptyTIme()).getBytes());
            }
            fos.write("\n-------------------------------------------------------\n\n".getBytes());
            fos.write("LOGGED INFORMATION:".getBytes());
            for(String S: record) {
                fos.write(("\n" + S).getBytes());
            }
        } catch (Exception e) {
            System.out.println("This should never print");
        }

    }

}
