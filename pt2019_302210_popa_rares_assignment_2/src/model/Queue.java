package model;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Queue implements Runnable {
    private int queueID;
    private int maxClients;
    private double waitRate;
    private int totalWait;
    private int emptyTime;
    private int clientsProcessed;
    private int elapsedTime;
    private double simulationSpeed;

    private boolean closeThread = false;

    private BlockingQueue<Client> clientList;

    private VBox visualQueue;

    Queue(int nr, int startTime, double simSpeed) {
        waitRate = 0;
        totalWait = 0;
        emptyTime = 0;
        clientsProcessed = 0;
        queueID = nr;
        elapsedTime = startTime;
        simulationSpeed = simSpeed;
        clientList = new LinkedBlockingQueue<>(Server.MAX_QUEUE_CAPACITY);
        maxClients = Server.MAX_QUEUE_CAPACITY;
    }

    public void run() {
        while ((elapsedTime <= 720 || !clientList.isEmpty()) && !closeThread) {
            if (!isEmpty()) {
                try {
                    decreaseTime();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            elapsedTime++;
            if (clientList.size() == 0) {
                emptyTime++;
            }
            Platform.runLater(() -> {
                if (!visualQueue.getChildren().isEmpty()) {
                    ProgressIndicator P = (ProgressIndicator) visualQueue.getChildren().get(0);
                    P.setProgress(1 - clientList.peek().progressRate());
                    if (elapsedTime > 720) {
                        P.setStyle("-fx-accent: red");
                    }
                }
            });

            try {
                Thread.sleep((long) ((11 - simulationSpeed) * 100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void addClient(Client C) {
        if (clientList.size() < maxClients) {
            clientList.add(C);
            Platform.runLater(() -> {
                ProgressIndicator P = new ProgressIndicator(0);
                bindTooltip(P, new Tooltip(C.toString()));
                visualQueue.getChildren().add(P);
            });
        }
    }

    private void decreaseTime() throws InterruptedException {
        for (Client C : clientList) {
            C.totalWaitingTime++;
        }
        if (!clientList.peek().decreaseTime()) {
            waitRate += clientList.peek().waitRate();
            clientsProcessed++;
            Client C = clientList.take();
            totalWait += C.totalWaitingTime;

            Platform.runLater(() -> visualQueue.getChildren().remove(0));

            //System.out.println("Client " + C + " has EXITED queue " + queueID + ".");
            Logger.add("Client " + C + " has EXITED queue " + queueID + ".", elapsedTime);
        }
    }

    int emptySlots() {
        return maxClients - clientList.size();
    }

    private boolean isEmpty() {
        return clientList.isEmpty();
    }

    int getQueueNr() {
        return queueID;
    }

    int getProcessed() {
        return clientsProcessed;
    }

    int getNrClients() {
        return clientList.size();
    }

    double getSpeed() {
        double retVal = waitRate / clientsProcessed;
        if (retVal > 1) {
            return 1;
        }
        return retVal;
    }

    int getEmptyTIme() {
        return emptyTime;
    }

    double averageWait() {
        return (double) totalWait / clientsProcessed;
    }

    public void linkQueue(VBox v) {
        visualQueue = v;
    }

    void close() {
        closeThread = true;
    }

    public String toString() {
        return "QUEUE " + queueID + ": " + clientList + " COUNT: " + clientList.size() + "\nSPEED: " + waitRate / clientsProcessed + "\nPROCESSED: " + clientsProcessed + '\n';
    }

    //function eliminates tooltip default delay, making the UI more responsive
    //Author StackOverflow user Aleks Messier
    private static void bindTooltip(final Node node, final Tooltip tooltip) {
        node.setOnMouseMoved(event -> {
            // +15 moves the tooltip 15 pixels below the mouse cursor;
            // if you don't change the y coordinate of the tooltip, you
            // will see constant screen flicker
            tooltip.show(node, event.getScreenX(), event.getScreenY() + 15);
        });
        node.setOnMouseExited(event -> tooltip.hide());
    }
}
