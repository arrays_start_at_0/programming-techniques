package model;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class Server implements Runnable {
    ///simmulation settings
    static int MIN_SERVICE_TIME;
    static int MAX_SERVICE_TIME;
    static int MIN_ARRIVAL_TIME;
    static int MAX_ARRIVAL_TIME;
    static int MAX_QUEUE_CAPACITY;
    static int QUEUE_NUMBER;
    static int CLIENT_NUMBER;
    static final String[] CLIENT_NAMES = {"Liviu", "Cezara", "Alexei", "Cristi", "Grig", "Rares", "Mădă", "Iulia", "Maria", "Marcel", "Giosan", "Felix"};
    //---------------------------------------------------------------------
    private ProgressBar bar;
    private Text timeText;
    private Text statusText;

    private int counter;
    private double speed;//1-10
    private int peakTime;
    private int maxIdle = -1;
    private int clientsInSystem;

    private boolean closeThread = false;

    private ArrayList<Queue> queueList;
    private PriorityQueue<Client> expectedClients;
    private PriorityQueue<Client> postponedClients;
    private ArrayList<Client> toInsert;

    public Server(int nrQueues, int nrClients, double simSpeed, int queCapacity, int minService, int maxService, int minArrival, int maxArrival) {
        counter = 0;
        speed = simSpeed;

        MAX_QUEUE_CAPACITY = queCapacity;
        MIN_SERVICE_TIME = minService;
        MAX_SERVICE_TIME = maxService;
        MIN_ARRIVAL_TIME = minArrival;
        MAX_ARRIVAL_TIME = maxArrival;
        QUEUE_NUMBER = nrQueues;
        CLIENT_NUMBER = nrClients;

        queueList = new ArrayList<>();
        expectedClients = new PriorityQueue<>(new ClientComparator());
        postponedClients = new PriorityQueue<>(new ClientComparator());
        toInsert = new ArrayList<>();
        populateShop(nrClients, nrQueues);

    }

    private void populateShop(int nrClients, int nrQueues) {
        for (int i = 0; i < nrClients; i++) {
            expectedClients.add(new Client());
        }
        System.out.println(expectedClients);
        for (int i = 0; i < nrQueues; i++) {
            queueList.add(new Queue(i + 1, counter, speed));
        }
    }

    private void startQueues() {
        for (Queue Q : queueList) {
            Thread t = new Thread(Q);
            t.start();
        }
    }

    public void linkTime(ProgressBar b, Text time) {
        bar = b;
        timeText = time;
    }

    public void linkStatus(Text status) {
        statusText = status;
    }

    public void run() {
        while (!closeThread) {
            if (counter == 0) {
                startQueues();
            }

            addPostponed();
            toInsert = getNewClients(counter);
            if (!toInsert.isEmpty()) {
                addToQueue(toInsert);
            }

            counter++;

            Platform.runLater(() -> {
                bar.setProgress((double) counter / 720);
                if (counter > 720) {
                    bar.setStyle("-fx-accent: red");
                }

                clientsInSystem = 0;
                makeTime();
                makeStatus();
                clientsInSystem += postponedClients.size();

                if (maxIdle < clientsInSystem) {
                    maxIdle = clientsInSystem;
                    peakTime = counter;
                }
            });

            try {
                Thread.sleep((long) ((11 - speed) * 100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<Client> getNewClients(int currentTime) {//needs to return more than one and in O(1)
        ArrayList<Client> retClient = new ArrayList<>();
        while (!expectedClients.isEmpty() && expectedClients.peek().getArrivalTime() == currentTime) {
            retClient.add(expectedClients.poll());
        }
        return retClient;
    }

    private void addToQueue(ArrayList<Client> toInsert) {//needs to happen in O(1) to avoid desync
        Queue Q = getSmallestQueue();
        while (!toInsert.isEmpty() && Q != null) {
            Client C = toInsert.get(0);
            toInsert.remove(0);
            //System.out.println("Client " + C + " has been INSERTED to queue " + Q.getQueueNr() + ".");
            Logger.add("Client " + C + " has been INSERTED to queue " + Q.getQueueNr() + ".", counter);
            Q.addClient(C);

            Q = getSmallestQueue();
        }
        while (!toInsert.isEmpty()) {
            Client C = toInsert.get(0);
            toInsert.remove(0);
            //System.out.println("Client " + C + " has been POSTPONED");
            Logger.add("Client " + C + " has been POSTPONED", counter);
            postponedClients.add(C);
        }
    }

    private void addPostponed() {
        Queue Q = getSmallestQueue();
        while (!postponedClients.isEmpty() && Q != null) {
            Client C = postponedClients.poll();
            //System.out.println("Client " + C + " has been POST-INSERTED to queue " + Q.getQueueNr() + ".");
            Logger.add("Client " + C + " has been POST-INSERTED to queue " + Q.getQueueNr() + ".", counter);
            Q.addClient(C);

            Q = getSmallestQueue();
        }
    }

    private Queue getSmallestQueue() {
        Queue chosenQ = null;
        int max = 0;
        for (Queue Q : queueList) {
            if (max < Q.emptySlots()) {
                max = Q.emptySlots();
                chosenQ = Q;
            }
        }
        return chosenQ;
    }

    public ArrayList<Queue> getQueues() {
        return queueList;
    }

    public int getnrQueues() {
        return queueList.size();
    }

    public int getQueueCap() {
        return MAX_QUEUE_CAPACITY;
    }

    int getPeakTime() {
        return peakTime;
    }

    private void makeTime() {
        String H = String.valueOf(counter / 60 + 8);
        String M = String.valueOf(counter % 60);
        if (H.length() == 1) {
            H = "0" + H;
        }
        if (M.length() == 1) {
            M = "0" + M;
        }
        timeText.setText(H + ":" + M);
    }

    private void makeStatus() {
        String s = "";
        s += "Clients yet to arrive: ";
        s += (expectedClients.size()) + "\n";

        if (!expectedClients.isEmpty()) {
            s += "Next client in: " + (expectedClients.peek().getArrivalTime() - counter) + "\n";
        }

        s += "Clients not fitted into any queue: ";
        s += postponedClients.size() + "\n";

        int totalProcessed = 0;
        String qString = "";
        double maxSpeed = 0;
        int fastQ = -1;
        boolean closeQueues = true;
        for (Queue Q : queueList) {
            clientsInSystem += Q.getNrClients();
            if (Q.getNrClients() != 0 || counter <= 720) {
                closeQueues = false;
            }
            totalProcessed += Q.getProcessed();
            double speed = Q.getSpeed();
            if (maxSpeed < speed) {
                fastQ = Q.getQueueNr();
                maxSpeed = speed;
            }
            if (Q.getSpeed() > 0) {
                qString += "Q" + Q.getQueueNr() + ": " + speed + " People: " + Q.getNrClients() + "\n";
            }
        }
        closeThread = closeQueues;
        if(closeThread) { close(); }

        s += "Clients processed: " + totalProcessed + "\n";
        if (fastQ != -1) {
            s += "Fastest queue: Queue " + fastQ + " [" + maxSpeed + "]\n";
        }
        s += "\n";
        s += qString;

        statusText.setText(s);
    }

    public void close() {
        closeThread = true;
        Logger.add("END OF SIMULATION", counter);
        for (Queue Q : queueList) {
            Q.close();
        }
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("SIMULATION HAS ENDED");
            alert.setHeaderText("Simulation has ended in " + counter +" steps." +
                                "\nIt has proccessed " + CLIENT_NUMBER + " clients, " +
                                "\nwith an average waiting time of " + calculateAvgWait() +
                                "\nand an average speed of " + calculateAvgSpeed() + "." +
                                "\nPeak time was around " + peakTime + ".");
            alert.show();
        });
    }

    public boolean isRunning() {
        return !closeThread;
    }

    double calculateAvgWait() {
        double avgWait = 0;
        for(Queue Q: queueList) {
            avgWait += Q.averageWait();
        }
        return avgWait / getnrQueues();
    }

    double calculateAvgSpeed() {
        double avgSpeed = 0;
        for(Queue Q: queueList) {
            avgSpeed += Q.getSpeed();
        }
        return avgSpeed / getnrQueues();
    }

    public String toString() {
        String s = "Current time: " + counter + "\n";
        for (int i = 0; i < queueList.size(); i++) {
            s += "Queue " + (i + 1) + ": " + queueList.get(i) + "\n";
        }
        s += "--------------------------------------------------\n";
        return s;
    }
}
