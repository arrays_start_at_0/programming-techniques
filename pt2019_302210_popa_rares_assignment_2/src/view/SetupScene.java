package view;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Server;


public class SetupScene extends Application {
    public static void main(String[] args) { launch(args); }

    private Stage mainStage;
    private TextField queues;
    private TextField queueCap;
    private TextField clients;
    private TextField minService;
    private TextField maxService;
    private TextField minArrival;
    private TextField maxArrival;
    private Slider speedSlider;

    @Override
    public void start(Stage settingsPage) throws Exception {
        settingsPage.setTitle("Simulation setup");
        Text setUpText = new Text ("Please input the simulation parameters");
        setUpText.setFont(Font.font("Verdana", FontWeight.BOLD, 16));
        //--------------------------------------------------------------------
        Text nrQueuesText = new Text("Number of queues: ");
        queues = new TextField();
        queues.setPromptText("3");
        HBox.setHgrow(queues, Priority.ALWAYS);
        HBox queuesLayout = new HBox(10);
        queuesLayout.getChildren().addAll(nrQueuesText, queues);
        //--------------------------------------------------------------------
        Text queueCapText = new Text("Queue capacity: ");
        queueCap = new TextField();
        queueCap.setPromptText("5");
        HBox.setHgrow(queueCap, Priority.ALWAYS);
        HBox capLayout = new HBox(10);
        capLayout.getChildren().addAll(queueCapText, queueCap);
        //---------------------------------------------------------------------
        Text nrClientsText = new Text("Number of clients: ");
        clients = new TextField();
        clients.setPromptText("100");
        HBox.setHgrow(clients, Priority.ALWAYS);
        HBox clientLayout = new HBox(10);
        clientLayout.getChildren().addAll(nrClientsText, clients);
        //---------------------------------------------------------------------
        Text minServiceText = new Text("Minimum service time: ");
        minService = new TextField();
        minService.setPromptText("5");
        VBox minServiceLayout = new VBox(10);
        minServiceLayout.setAlignment(Pos.CENTER);
        minServiceLayout.getChildren().addAll(minServiceText, minService);
        //--------------------------------------------------------------------
        Text maxServiceText = new Text("Maximum service time: ");
        maxService = new TextField();
        maxService.setPromptText("15");
        VBox maxServiceLayout = new VBox(10);
        maxServiceLayout.setAlignment(Pos.CENTER);
        maxServiceLayout.getChildren().addAll(maxServiceText, maxService);
        //--------------------------------------------------------------------
        HBox serviceLayout = new HBox(10);
        serviceLayout.getChildren().addAll(minServiceLayout, maxServiceLayout);
        //--------------------------------------------------------------------
        Text minArrivalText = new Text("Minimum arrival time: ");
        minArrival = new TextField();
        minArrival.setPromptText("0");
        VBox minArrivalLayout = new VBox(10);
        minArrivalLayout.setAlignment(Pos.CENTER);
        minArrivalLayout.getChildren().addAll(minArrivalText, minArrival);
        //--------------------------------------------------------------------
        Text maxArrivalText = new Text("Maximum arrival time: ");
        maxArrival = new TextField();
        maxArrival.setPromptText("720");
        VBox maxArrivalLayout = new VBox(10);
        maxArrivalLayout.setAlignment(Pos.CENTER);
        maxArrivalLayout.getChildren().addAll(maxArrivalText, maxArrival);
        //--------------------------------------------------------------------
        HBox arrivalLayout = new HBox(10);
        arrivalLayout.getChildren().addAll(minArrivalLayout, maxArrivalLayout);
        //--------------------------------------------------------------------
        Text simSpeedText = new Text("Simulation speed");
        speedSlider = new Slider();
        speedSlider.setMin(1);
        speedSlider.setMax(10);
        speedSlider.setShowTickLabels(true);
        speedSlider.setValue(5.5);
        VBox speedLayout = new VBox(10);
        speedLayout.getChildren().addAll(simSpeedText, speedSlider);
        //------------------------------------------------------------------------
        Button startButton = new Button("START");
        startButton.setOnAction(e -> startSim());
        //------------------------------------------------------------------------
        VBox mainLayout = new VBox(15);
        mainLayout.setAlignment(Pos.CENTER);
        mainLayout.setPadding(new Insets(5, 20, 20, 20));
        mainLayout.getChildren().addAll(setUpText, queuesLayout, capLayout, clientLayout, serviceLayout, arrivalLayout, speedLayout, startButton);

        Scene scene = new Scene(mainLayout, 400, 450);
        settingsPage.setScene(scene);
        settingsPage.show();
        mainStage = settingsPage;
    }

    private void startSim() {
        SimulationScene S;
        int nrQ, qCap, nrC, minS, maxS, minA, maxA;
        double speed;

        try {
            nrQ = Integer.valueOf(queues.getText());
            qCap = Integer.valueOf(queueCap.getText());
            nrC = Integer.valueOf(clients.getText());
            minS = Integer.valueOf(minService.getText());
            maxS = Integer.valueOf(maxService.getText());
            minA = Integer.valueOf(minArrival.getText());
            maxA = Integer.valueOf(maxArrival.getText());
            speed = speedSlider.getValue();

            if(minA >= maxA) { throw new IllegalArgumentException(); }
            if(minS >= maxS) { throw new IllegalArgumentException(); }
            if(nrQ <= 0 || nrC <= 0 || minS <= 0 || minA < 0 || maxA > 720 || qCap <= 0) { throw new IllegalArgumentException(); }

            Server server = new Server(nrQ, nrC, speed, qCap, minS, maxS, minA, maxA);
            S = new SimulationScene(server);
            S.start(mainStage);

        } catch (Exception e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText("Invalid inputs");
            alert.showAndWait();
        }
    }
}
