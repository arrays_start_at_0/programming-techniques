package view;

import javafx.application.Application;
import javafx.event.Event;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Logger;
import model.Queue;
import model.Server;

import java.util.ArrayList;


public class SimulationScene extends Application {
    private Scene simulationScene;
    private Server server;
    Thread sim;

    private int Height, Width;

    private ProgressBar timeProgress;
    private Text time;
    private Text status;
    private TextArea log;

    SimulationScene(Server s) {
        Width = 600;
        Height = (s.getQueueCap()) > 5 ? 900 : 100 * s.getQueueCap() + 400;

        server = s;
        timeProgress = new ProgressBar();
        timeProgress.setProgress(0);
        time = new Text("08:00");
        s.linkTime(timeProgress, time);
        status = new Text();
        s.linkStatus(status);
        log = new TextArea();
        Logger.linkLog(log);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        sim = new Thread(server);
        sim.start();

        HBox timeLayout = new HBox(10);
        timeProgress.prefWidthProperty().bind(timeLayout.widthProperty());
        timeLayout.getChildren().addAll(timeProgress, time);
        //-----------------------------------------------------------------------------------
        HBox queueLayout = makeQueueLayout();
        queueLayout.setAlignment(Pos.CENTER);
        int height = server.getQueueCap() * 65;
        if (height > 5 * 65) {
            height = 5 * 65;
        }
        queueLayout.setMinHeight(height);
        ScrollPane queueScroll = new ScrollPane(queueLayout);
        queueScroll.setFitToWidth(true);
        queueScroll.setPrefViewportHeight(height);
        //-----------------------------------------------------------------------------------
        HBox statusLayout = new HBox(10);
        ScrollPane statusScroll = new ScrollPane(status);
        statusScroll.setPrefViewportHeight(200);
        statusScroll.setPrefViewportWidth(290);
        statusLayout.getChildren().add(statusScroll);
        //-----------------------------------------------------------------------------------
        VBox buttonLayout = new VBox(5);
        Button stop = new Button("Stop");
        Button save = new Button("Save log");
        stop.setMinWidth(280);
        save.setMinWidth(280);
        stop.setMinHeight(60);
        save.setMinHeight(60);
        stop.setOnAction(e -> stopSimulation());
        save.setOnAction(e -> saveSim());
        buttonLayout.getChildren().addAll(stop, save);
        buttonLayout.setAlignment(Pos.CENTER_RIGHT);
        //-----------------------------------------------------------------------------------
        HBox midLayout = new HBox(10);
        midLayout.getChildren().addAll(statusLayout, buttonLayout);
        //-----------------------------------------------------------------------------------
        log.setPrefWidth(600);
        log.setPrefRowCount(10);
        VBox.setVgrow(log, Priority.ALWAYS);
        //-----------------------------------------------------------------------------------
        VBox mainLayout = new VBox(10);
        mainLayout.getChildren().addAll(timeLayout, queueScroll, midLayout, log);
        mainLayout.setAlignment(Pos.TOP_CENTER);
        mainLayout.setPadding(new Insets(5, 10, 20, 10));

        simulationScene = new Scene(mainLayout, Width, Height);
        primaryStage.setScene(simulationScene);
        primaryStage.setTitle("Simulation");


        primaryStage.getScene().getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closeWindowEvent);
    }

    private HBox makeQueueLayout() {
        HBox ret = new HBox(2);
        for (int i = 0; i < server.getnrQueues(); i++) {
            VBox queue = new VBox();
            server.getQueues().get(i).linkQueue(queue);
            queue.setStyle("-fx-border-style: solid inside;" +
                    "-fx-border-width: 1;" +
                    "-fx-border-insets: 2;" +
                    "-fx-border-radius: 2;");
            queue.setPrefWidth(500);
            ret.getChildren().add(queue);
        }
        return ret;
    }

    private void stopSimulation() {
        server.close();
    }

    private void saveSim() {
        if (!server.isRunning()) {
            Logger.saveLog(server);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("SUCCESS");
            alert.setHeaderText("Log file created successfully.");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("ALERT");
            alert.setHeaderText("Cannot generate log while the simulation is still running.");
            alert.show();
        }
    }

    private <T extends Event> void closeWindowEvent(T t) {
        System.out.println("GOODBYE");
        server.close();
    }

}


