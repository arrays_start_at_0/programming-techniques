package ExceptionPackage;

public class MultiplePolynomialLettersException extends Exception {
    public MultiplePolynomialLettersException(String varA, int posA, String varB, int posB) {
        super("Multiple variables used: " + varA + " and " + varB +
               " (positions: " + posA + " and  " + posB + ").");
    }
}
