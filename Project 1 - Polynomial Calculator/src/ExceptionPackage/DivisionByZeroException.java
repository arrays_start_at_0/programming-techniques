package ExceptionPackage;

public class DivisionByZeroException extends Exception {
    public DivisionByZeroException () {
        super("Division by 0.");
    }
}