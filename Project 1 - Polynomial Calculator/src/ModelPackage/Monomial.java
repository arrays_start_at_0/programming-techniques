package ModelPackage;

public class Monomial {
    private int power;
    private double coefficient;
    private boolean isIntegrated = false;
    String variable;

    Monomial (Monomial M) {//copy constructor
        power = M.power;
        coefficient = M.coefficient;
        variable = M.variable;
        isIntegrated = M.isIntegrated;
    }

    Monomial(String var) { //creates an integrated Monomial
        variable = var;
        power = 0;
        coefficient = 1;
        isIntegrated = true;
    }

    Monomial(int pow, double coef, String var) {
        power = pow;
        coefficient = coef;
        variable = var;
    }

    Monomial(String expression, String variable) {
        this.variable = variable;
        int varLocation = expression.indexOf(variable);

        String leftOfVar = varLocation != -1 ? expression.substring(0, varLocation) : expression;
        String rightOfVar = varLocation != -1 && varLocation + variable.length() < expression.length() ? expression.substring(varLocation + variable.length() + 1) : "";

        if (leftOfVar.isEmpty()) {
            coefficient = 1;
        } else if (leftOfVar.length() == 1 && (leftOfVar.charAt(0) == '+' || leftOfVar.charAt(0) == '-')) {
            coefficient = Double.valueOf(leftOfVar + "1");
        } else {
            coefficient = Double.valueOf(leftOfVar);
        }

        if (rightOfVar.isEmpty()) {
            if (varLocation == -1) {
                power = 0;
            } else {
                power = 1;
            }
        } else {
            power = Integer.valueOf(rightOfVar);
        }
    }

    int getPower() {
        return power;
    }
    double getCoefficient() { return coefficient; }

    void scalarProduct(double scalar) {
        coefficient *= scalar;
    }

    void derive() {
        coefficient *= power;
        power--;
    }

    void integrate() {
        power++;
        coefficient /= power;
    }

    public String toString() {
        if(coefficient == 0) { return ""; }
        String s = coefficient >= 0 ? "+ " : "- ";
        if ((coefficient != 1 && coefficient != -1 || power == 0) && !isIntegrated) {
            s += Math.abs(coefficient);
        }
        if (isIntegrated) {
            s += "C";
        }
        switch (power) {
            case 0:
                break;
            case 1:
                s += variable;
                break;
            default:
                s += variable + "^" + power;
        }
        return s + " ";
    }
}
