package TestPackage;

import ExceptionPackage.*;
import junit.framework.*;
import ModelPackage.*;
import java.util.Arrays;

public class OperationTest extends TestCase {
    Polynomial P, P1, R;

    public void setUp() {
        try {
            P = new Polynomial("2x^3+5x-7");
            P1 = new Polynomial("2x^3 +  5x - 7 + 0x^0");
            R = new Polynomial("x^2+7");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testParse() {
        TestCase.assertEquals(P.toString(), P1.toString());
    }

    public void testAdd() throws DifferentPolynomialVariableException {
        TestCase.assertEquals(Operation.add(P, R).toString(), "2.0x^3 + x^2 + 5.0x ");
    }

    public void testSub() throws DifferentPolynomialVariableException {
        TestCase.assertEquals(Operation.sub(P, R).toString(), "2.0x^3 - x^2 + 5.0x - 14.0 ");
    }

    public void testDerivation() {
        Polynomial copy = P.copy();
        TestCase.assertEquals(Operation.derive(copy).toString(), "6.0x^2 + 5.0 ");
    }

    public void testIntegration() {
        Polynomial copy = P.copy();
        TestCase.assertEquals(Operation.integrate(copy).toString(), "0.5x^4 + 2.5x^2 - 7.0x + C ");
    }

    public void testProduct() throws DifferentPolynomialVariableException {
        TestCase.assertEquals(Operation.product(P, R).toString(), "2.0x^5 + 19.0x^3 - 7.0x^2 + 35.0x - 49.0 ");
    }

    public void testDivision() throws DivisionByZeroException, DifferentPolynomialVariableException {
        TestCase.assertEquals(Arrays.toString(Operation.division(P, R)), "[2.0x , -9.0x - 7.0 ]");//quotient and reminder
    }
}
