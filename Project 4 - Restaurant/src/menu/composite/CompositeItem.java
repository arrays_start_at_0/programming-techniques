package menu.composite;

import exceptions.InvalidItemPairException;
import exceptions.InvalidItemTypeException;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeItem implements MenuItem, Serializable {

    private ArrayList<MenuItem> items;
    private String type;

    public CompositeItem(MenuItem I1, MenuItem I2) throws InvalidItemPairException, InvalidItemTypeException {
        items = new ArrayList<>();
        add(I1);
        add(I2);
    }

    public void add (MenuItem I) throws InvalidItemPairException, InvalidItemTypeException {
        if(I instanceof BaseItem && !((BaseItem) I).isPairable()) { throw new InvalidItemPairException((BaseItem)I); }

        if(type == null) {
            type = I.getType();
        } else if(!type.equals(I.getType())) {
            throw new InvalidItemTypeException(I, type);
        }

        if(I instanceof BaseItem) {
            items.add(I);
        } else {
            CompositeItem C = (CompositeItem) I;
            items.addAll(C.items);
        }
    }

    @Override
    public String getName() {
        String s = items.get(0).getName() + " cu " + items.get(1).getName();
        if(items.size() > 2) {
            s += " și ";
            for(int  i = 2; i < items.size(); i++) {
                s += items.get(i).getName() + ", ";
            }
            return s.substring(0, s.length() - 2);
        }
        return s;
    }

    @Override
    public double getPrice() {
        double price = 0;
        for(MenuItem I : items) {
            price += I.getPrice();
        }
        return price;
    }

    @Override
    public String getType() { return type; }

    @Override
    public String getQuantity() {
        double quanta = 0;
        for(MenuItem I : items) {
            if(I instanceof FoodItem) {
                quanta += ((FoodItem) I).getWeight();
            } else {
                quanta += ((DrinkItem) I).getVolume();
            }
        }
        if(type.equals("FOOD")) {
            return quanta + "g";
        } else {
            return quanta + "ml";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeItem that = (CompositeItem) o;
        return items.equals(that.items) &&
                type.equals(that.type);
    }

    @Override
    public String toString() {
        return  "Item type: " + items.get(0).getType() +
                "\n\tName: " + getName() +
                "\n\tPrice: " + getPrice();
    }
}
