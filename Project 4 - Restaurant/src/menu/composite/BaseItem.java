package menu.composite;

import java.io.Serializable;
import java.util.Objects;

public class BaseItem implements MenuItem, Serializable {
    private String type;
    private String name;
    private double price;
    private boolean pairable;

    BaseItem(String type, String name, double price, boolean isPairable) {
        this.type = type;
        this.name = name;
        this.price = price;
        pairable = isPairable;
    }

    public String getName() { return name; }
    public double getPrice() { return price; }
    public String getType() { return type; }

    @Override
    public String getQuantity() {
        if(type.equals("FOOD")) {
            return ((FoodItem) this).getWeight() + "g";
        } else {
            return ((DrinkItem) this).getVolume() + "ml";
        }
    }

    public boolean isPairable() {
        System.out.println(name + " " + pairable);
        return pairable; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseItem baseItem = (BaseItem) o;
        return Double.compare(baseItem.price, price) == 0 &&
                pairable == baseItem.pairable &&
                type.equals(baseItem.type) &&
                name.equals(baseItem.name);
    }

    @Override
    public String toString() {
        return "Item type: " + getType() +
                "\n\tName: " + getName() +
                "\n\tPrice: " + getPrice();
    }
}
