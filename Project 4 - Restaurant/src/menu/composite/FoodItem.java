package menu.composite;

public class FoodItem extends BaseItem{
    private double weight;

    public FoodItem(String name, double price, boolean isPairable, double weight) {
        super("FOOD", name, price, isPairable);
        this.weight = weight;
    }

    public FoodItem(String name, double price, double weight) {//default is true
        super("FOOD", name, price, true);
        this.weight = weight;
    }
    double getWeight() { return weight; }

    @Override
    public String toString() {
        return  super.toString() +
                "\n\tWeight: " + getWeight() + "\n";
    }
}
