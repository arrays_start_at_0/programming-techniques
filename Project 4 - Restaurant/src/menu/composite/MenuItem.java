package menu.composite;

public interface MenuItem {

    String getName();
    double getPrice();
    String getType();
    String getQuantity();
    String toString();

}
