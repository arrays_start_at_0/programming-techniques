package menu;

import javafx.scene.control.Spinner;
import menu.composite.FoodItem;
import menu.composite.MenuItem;

import java.io.Serializable;

public class MenuItemWrapper implements MenuItem {
    private Spinner<Integer> spinner;
    private MenuItem I;

    public MenuItemWrapper(MenuItem item) {
        spinner = new Spinner<>(0, 100, 0);
        I = item;
    }

    @Override
    public String getName() { return I.getName(); }
    public double getPrice() { return I.getPrice(); }
    public String getType() { return I.getType(); }
    public String getQuantity() { return I.getQuantity(); }
    public Spinner getSpinner() { return spinner; }
    public int getAmmount() { return spinner.getValue(); }
}
