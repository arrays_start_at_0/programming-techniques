package exceptions;


import menu.composite.MenuItem;

public class InvalidItemTypeException extends Exception{
    public InvalidItemTypeException(MenuItem I, String type) {
        super("Selected item " + I.getName() + " is of type " + I.getType() + ". Expected type: " + type);
    }
}
