package exceptions;

public class EmptyOrderException extends Exception {
    public EmptyOrderException() { super("No order items selected."); }
}
