package exceptions;

import menu.composite.BaseItem;

public class InvalidItemPairException extends Exception {
    public InvalidItemPairException(BaseItem I) {
        super("Selected item cannot be paired: " + I.getName() + ".");
    }
}
