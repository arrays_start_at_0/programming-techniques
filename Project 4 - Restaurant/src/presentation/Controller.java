package presentation;

import exceptions.EmptyOrderException;
import file_writer.Receipt;
import javafx.application.Application;
import javafx.stage.Stage;
import menu.MenuItemWrapper;
import menu.composite.MenuItem;
import model.Order;
import model.Restaurant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Controller extends Application {
    private Restaurant R;

    private Stage appStage;
    private MainMenu main;
    private OrderMenu order;
    private ChefOps chef;
    private EditMenu edit;

    public static void main(String[] args)  { launch(args); }
    @Override
    public void start(Stage primaryStage){
        R = new Restaurant();
        main = new MainMenu(this);
        order = new OrderMenu(this);
        chef = new ChefOps(this);
        edit = new EditMenu(this);
        R.addObserver(chef);
        appStage = primaryStage;
        main.start(appStage);
    }

    void launchMenu() {
        main.start(appStage);
    }
    void launchOrder() { order.start(appStage); }
    void launchEdit() { edit.start(appStage); }
    void launchChef() {
        chef.start(new Stage());
    }

    ArrayList<Order> getOrders() { return R.getOrders(); }

    ArrayList<MenuItem> getMenu() { return R.getMenu(); }
    void addToMenu(MenuItem I) { R.addItem(I); }
    void updateMenu(MenuItem oldItem, MenuItem newItem) { R.updateItem(oldItem, newItem);}
    void removeFromMenu(List<MenuItem> items) { R.removeItems(items); }

    void saveMenu() {
        try {
            R.saveMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    ArrayList<MenuItem> search(String key) {
        ArrayList<MenuItem> retVal = new ArrayList<>();
        ArrayList<MenuItem> menu = R.getMenu();

        for(MenuItem I : menu) {
            if(I.getName().indexOf(key) != -1) {
                retVal.add(I);
            }
        }

        return retVal;
    }

    void newOrder(List<MenuItemWrapper> items) throws EmptyOrderException {
        R.newOrder(items);
    }

    void makeReceipt(model.Order O) {
        Receipt.writeReceipt(O);
    }
}
