package presentation;

import exceptions.EmptyOrderException;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import menu.MenuItemWrapper;
import menu.composite.MenuItem;

import java.util.ArrayList;

public class OrderMenu extends Application {
    private ArrayList<MenuItem> shoppingCart;
    private Controller C;

    private TextField searchBox;
    private TextField pageNumber;
    private ArrayList<MenuItem> menuList;
    private TableView<MenuItemWrapper> menuTable;

    private final int WIDTH = 800;
    private final int HEIGHT = 550;
    private int startIndex = 0;

    OrderMenu(Controller controller) {
        C = controller;
        shoppingCart = new ArrayList<>();
        menuList = new ArrayList();
    }

    @Override
    public void start(Stage orderStage) {
        Button backButton = new Button("←");
        backButton.setOnAction(e -> C.launchMenu());
        //---------------------------------------------------------
        Text searchText = new Text("Product name: ");
        searchBox = new TextField();
        Button searchButton = new Button("\uD83D\uDD0D");
        Button prevPage = new Button("←");
        pageNumber = new TextField("0");
        pageNumber.setMinWidth(30);
        pageNumber.setMaxWidth(30);
        pageNumber.setEditable(false);
        Button nextPage = new Button("→");

        searchButton.setOnAction(e -> search());
        prevPage.setOnAction(e -> changeStartIndex(true));
        nextPage.setOnAction(e -> changeStartIndex(false));

        HBox controlsLayout = new HBox(1);
        controlsLayout.setAlignment(Pos.CENTER_RIGHT);
        controlsLayout.getChildren().addAll(searchText, searchBox, searchButton, prevPage, pageNumber, nextPage);
        //---------------------------------------------------------
        menuTable = new TableView<>();
        menuTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        TableColumn<MenuItemWrapper, String> nameColumn = new TableColumn<>("NAME");
        TableColumn<MenuItemWrapper, String> typeColumn = new TableColumn<>("TYPE");
        TableColumn<MenuItemWrapper, String> quantityColumn = new TableColumn<>("WEIGHT/VOLUME");
        TableColumn<MenuItemWrapper, String> priceColumn = new TableColumn<>("PRICE");
        TableColumn<MenuItemWrapper, Spinner<Integer>> ammountColumn = new TableColumn<>("AMOUNT");

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        ammountColumn.setCellValueFactory(new PropertyValueFactory<>("spinner"));


        menuTable.getColumns().addAll(nameColumn, typeColumn, quantityColumn, priceColumn, ammountColumn);
        menuTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        menuTable.setMinHeight(425);

        populateTable();
        //---------------------------------------------------------
        Button makeOrder = new Button("Make order");
        makeOrder.setOnAction(e -> newOrder());
        HBox buttonBox = new HBox(makeOrder);
        buttonBox.setPadding(new Insets(5));
        buttonBox.setAlignment(Pos.CENTER);
        //---------------------------------------------------------
        VBox mainLayout = new VBox(5);
        mainLayout.getChildren().addAll(backButton, controlsLayout, menuTable, buttonBox);
        mainLayout.setPadding(new Insets(0, 5, 0, 5));
        Scene orderMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        orderStage.setScene(orderMenu);
        orderStage.setTitle("Make new Order");
        orderStage.show();
    }

    private void search() {
        String key = searchBox.getText();
        if(key.isEmpty()) {
            populateTable();
        } else {
            populateTable(C.search(key));
        }
        searchBox.setText("");
    }

    private void newOrder() {
        try {
            C.newOrder(menuTable.getSelectionModel().getSelectedItems());
            Alert a = new Alert(Alert.AlertType.INFORMATION);
            a.setTitle("Success");
            a.setHeaderText("Order created successfully!");
            a.showAndWait();
        } catch (EmptyOrderException e) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setHeaderText(e.getLocalizedMessage());
            a.showAndWait();
        }

    }

    private void populateTable() {
        menuList = C.getMenu();
        menuTable.getItems().clear();
        for(int i = startIndex; i < startIndex + 10 && i < menuList.size(); i++) {
            menuTable.getItems().add(new MenuItemWrapper(menuList.get(i)));
        }
    }

    private void populateTable(ArrayList<MenuItem> menuList) {
        menuTable.getItems().clear();
        for(int i = startIndex; i < startIndex + 10 && i < menuList.size(); i++) {
            menuTable.getItems().add(new MenuItemWrapper(menuList.get(i)));
        }
    }

    private void changeStartIndex(boolean back) {
        if (back) {
            if (startIndex - 10 >= 0) {
                startIndex -= 10;
                pageNumber.setText(String.valueOf(Integer.valueOf(pageNumber.getText()) - 1));
            }
        } else {
            if (startIndex + 10 <= menuList.size() - 1){
                startIndex += 10;
                pageNumber.setText(String.valueOf(Integer.valueOf(pageNumber.getText()) + 1));
            }
        }
        populateTable();
    }
}
