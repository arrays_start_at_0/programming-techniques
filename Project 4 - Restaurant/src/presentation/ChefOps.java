package presentation;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Order;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ChefOps extends Application implements Observer {
    private Controller C;
    private ArrayList<Order> orderList;
    private TableView<Order> orderTable;
    private TextField pageNumber;

    private final int WIDTH = 800;
    private final int HEIGHT = 410;
    private int startIndex = 0;

    ChefOps(Controller controller) {
        C = controller;
        orderList = new ArrayList();
        orderTable = new TableView<>();
    }

    @Override
    public void start(Stage chefStage) {
        Button receiptButton = new Button("\uD83D\uDCDD");
        receiptButton.setOnAction(e -> generateReceipt());
        Tooltip.install(receiptButton, new Tooltip("Generate receipt"));

        Pane spacer = new Pane();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        Button prevPage = new Button("←");
        pageNumber = new TextField("0");
        pageNumber.setMinWidth(30);
        pageNumber.setMaxWidth(30);
        pageNumber.setEditable(false);
        Button nextPage = new Button("→");

        prevPage.setOnAction(e -> changeStartIndex(true));
        nextPage.setOnAction(e -> changeStartIndex(false));

        HBox controlsLayout = new HBox(1);
        controlsLayout.setAlignment(Pos.CENTER_RIGHT);
        controlsLayout.getChildren().addAll(receiptButton, spacer, prevPage, pageNumber, nextPage);
        //--------------------------------------------
        TableColumn<Order, Integer> IDColumn = new TableColumn<>("ID");
        TableColumn<Order, String> nameColumn = new TableColumn<>("CONTENTS");
        TableColumn<Order, Double> priceColumn = new TableColumn<>("PRICE");

        IDColumn.setCellValueFactory(new PropertyValueFactory<>("ID"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("contents"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("total"));

        IDColumn.setSortType(TableColumn.SortType.ASCENDING);

        IDColumn.prefWidthProperty().bind(orderTable.widthProperty().multiply(0.1));
        nameColumn.prefWidthProperty().bind(orderTable.widthProperty().multiply(0.7));
        priceColumn.prefWidthProperty().bind(orderTable.widthProperty().multiply(0.2));

        orderTable.getColumns().addAll(IDColumn, nameColumn, priceColumn);
        orderTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        orderTable.setMaxHeight(330);



        //--------------------------------------------
        VBox mainLayout = new VBox(5);
        mainLayout.getChildren().addAll(controlsLayout, orderTable);
        mainLayout.setPadding(new Insets(0, 5, 0, 5));
        Scene chefMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        chefStage.initModality(Modality.WINDOW_MODAL);
        chefStage.setScene(chefMenu);
        chefStage.setTitle("Chef menu");
        chefStage.show();
    }

    private void generateReceipt() {
        Order O = orderTable.getSelectionModel().getSelectedItem();
        if(O != null) {
            C.makeReceipt(O);
            Alert a = new Alert(Alert.AlertType.INFORMATION);
            a.setTitle("Success");
            a.setHeaderText("Receipt created succesfully");
            a.showAndWait();
        }
    }

    private void changeStartIndex(boolean back) {
        if (back) {
            if (startIndex - 10 >= 0) {
                startIndex -= 10;
                pageNumber.setText(String.valueOf(Integer.valueOf(pageNumber.getText()) - 1));
            }
        } else {
            if (startIndex + 10 <= orderList.size() - 1){
                startIndex += 10;
                pageNumber.setText(String.valueOf(Integer.valueOf(pageNumber.getText()) + 1));
            }
        }
        populateTable();
    }

    private void populateTable() {
        orderTable.getItems().clear();
        for(int i = startIndex; i < startIndex + 10 && i < orderList.size(); i++) {
            orderTable.getItems().add(orderList.get(i));
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        orderList = C.getOrders();
        populateTable();
    }
}
