package presentation;

import exceptions.InvalidItemPairException;
import exceptions.InvalidItemTypeException;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import menu.composite.*;
import menu.composite.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class EditMenu extends Application {
    private Controller C;
    private ArrayList<MenuItem> menuList;
    private TableView<MenuItem> menuTable;
    private TextField pageNumber;

    private final int WIDTH = 800;
    private final int HEIGHT = 410;
    private int startIndex = 0;

    EditMenu(Controller controller) {
        C = controller;
        menuList = new ArrayList();
    }

    @Override
    public void start(Stage editStage) {
        Button backButton = new Button("←");
        backButton.setOnAction(e -> {C.saveMenu(); C.launchMenu();});
        //--------------------------------------------
        Button addButton = new Button("+");
        Button mixButton = new Button("\uD83C\uDF00");
        Tooltip.install(mixButton, new Tooltip("Mix items"));

        Button editButton = new Button("\uD83D\uDD27");
        Tooltip.install(editButton, new Tooltip("Edit item"));

        addButton.setOnAction(e -> showDialog(null));
        mixButton.setOnAction(e -> mixItems());
        editButton.setOnAction(e -> editItems());

        Pane spacer = new Pane();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        Button prevPage = new Button("←");
        pageNumber = new TextField("0");
        pageNumber.setMinWidth(30);
        pageNumber.setMaxWidth(30);
        pageNumber.setEditable(false);
        Button nextPage = new Button("→");

        prevPage.setOnAction(e -> changeStartIndex(true));
        nextPage.setOnAction(e -> changeStartIndex(false));

        HBox controlsLayout = new HBox(1);
        controlsLayout.setAlignment(Pos.CENTER_RIGHT);
        controlsLayout.getChildren().addAll(addButton, mixButton, editButton, spacer, prevPage, pageNumber, nextPage);
        //--------------------------------------------
        menuTable = new TableView<>();

        TableColumn<MenuItem, String> nameColumn = new TableColumn<>("NAME");
        TableColumn<MenuItem, String> typeColumn = new TableColumn<>("TYPE");
        TableColumn<MenuItem, String> quantityColumn = new TableColumn<>("WEIGHT/VOLUME");
        TableColumn<MenuItem, String> priceColumn = new TableColumn<>("PRICE");

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        quantityColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));


        menuTable.getColumns().addAll(nameColumn, typeColumn, quantityColumn, priceColumn);
        menuTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        menuTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        menuTable.setMaxHeight(325);

        populateTable();
        //--------------------------------------------
        VBox mainLayout = new VBox(5);
        mainLayout.getChildren().addAll(backButton, controlsLayout, menuTable);
        mainLayout.setPadding(new Insets(0, 5, 0, 5));
        Scene editMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        editStage.setScene(editMenu);
        editStage.setTitle("Edit menu");
        editStage.show();

        editMenu.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.DELETE) {
                List<MenuItem> items = menuTable.getSelectionModel().getSelectedItems();
                C.removeFromMenu(items);
                populateTable();
            }
        });
    }

    private void editItems() {
        MenuItem I = menuTable.getSelectionModel().getSelectedItem();
        if(I != null) {
            showDialog(I);
        }
    }

    private void showDialog(MenuItem I) {
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);

        Text nameText = new Text("Name:   ");
        TextField nameField = new TextField();

        Text typeText = new Text("Type:     ");
        ComboBox<String> typeCombo = new ComboBox<>();
        typeCombo.getItems().addAll("FOOD", "DRINK");
        typeCombo.getSelectionModel().selectFirst();

        Text priceText = new Text("Price:     ");
        TextField priceField = new TextField();

        Text quantityText = new Text("Weight: ");
        TextField quantityField = new TextField();

        CheckBox pairableCheck = new CheckBox("can be paired with other items");
        pairableCheck.setSelected(true);

        typeCombo.setOnAction(e -> {
            if(typeCombo.getSelectionModel().getSelectedItem().equals("FOOD")) {
                quantityText.setText("Weight: ");
            } else {
                quantityText.setText("Volume:");
            }
        });

        if(I != null) {
            nameField.setText(I.getName());
            typeCombo.getSelectionModel().select(I.getType());
            priceField.setText(String.valueOf(I.getPrice()));
            String s = I.getQuantity();
            s = s.substring(0, s.length() - (I.getType().equals("FOOD") ? 1 : 2));
            quantityField.setText(s);
            if(I.getType().equals("DRINK")) { quantityText.setText("Volume:"); }
            if(I instanceof CompositeItem) {
                pairableCheck.setSelected(true);
                pairableCheck.setDisable(true);
            } else {
                pairableCheck.setSelected(((BaseItem)I).isPairable());
            }
        }

        Button confirmButton = new Button();
        confirmButton.setText(I != null ? "Update item" : "Add item");
        confirmButton.setOnAction(e -> {
            if(!nameField.getText().isEmpty()) {
                try {
                    double price = Double.valueOf(priceField.getText());
                    double quantity = Double.valueOf(quantityField.getText());

                    MenuItem newItem;

                    if(typeCombo.getSelectionModel().getSelectedItem().equals("FOOD")) {
                        newItem = new FoodItem(nameField.getText(), price, pairableCheck.isSelected(), quantity);
                    } else {
                        newItem = new DrinkItem(nameField.getText(), price, pairableCheck.isSelected(), quantity);
                    }

                    if(I != null) {
                        C.updateMenu(I, newItem);
                    } else {
                        C.addToMenu(newItem);
                    }

                    populateTable();
                    dialogStage.close();
                } catch (Exception ex) {
                    //alert
                }
            }
        });

        VBox vbox = new VBox(new HBox(nameText, nameField),
                             new HBox(typeText, typeCombo),
                             new HBox(priceText, priceField),
                             new HBox(quantityText, quantityField),
                             pairableCheck,
                             confirmButton);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(5));

        dialogStage.setScene(new Scene(vbox, 300, 200));
        dialogStage.setTitle("New item");
        dialogStage.setResizable(false);
        dialogStage.show();
    }

    private void mixItems() {
        ObservableList<MenuItem> selected = menuTable.getSelectionModel().getSelectedItems();
        if(selected.size() < 2) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Error");
            a.setHeaderText("Please select at least two items.");
            a.showAndWait();
        } else {
            try {
                CompositeItem I = new CompositeItem(selected.get(0), selected.get(1));
                for(int i = 2; i < selected.size(); i++) {
                    I.add(selected.get(i));
                }
                C.addToMenu(I);
                populateTable();
            } catch (InvalidItemPairException | InvalidItemTypeException e) {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Error");
                a.setHeaderText(e.getLocalizedMessage());
                a.showAndWait();
            }
        }
    }

    private void changeStartIndex(boolean back) {
        if (back) {
            if (startIndex - 10 >= 0) {
                startIndex -= 10;
                pageNumber.setText(String.valueOf(Integer.valueOf(pageNumber.getText()) - 1));
            }
        } else {
            if (startIndex + 10 <= menuList.size() - 1){
                startIndex += 10;
                pageNumber.setText(String.valueOf(Integer.valueOf(pageNumber.getText()) + 1));
            }
        }
        populateTable();
    }

    private void populateTable() {
        menuList = C.getMenu();
        menuTable.getItems().clear();
        for(int i = startIndex; i < startIndex + 10 && i < menuList.size(); i++) {
            menuTable.getItems().add(menuList.get(i));
        }
    }
}
