package model;

import exceptions.EmptyOrderException;
import menu.MenuItemWrapper;
import menu.composite.MenuItem;

import java.io.*;
import java.util.*;

public class Restaurant extends Observable implements RestaurantProcessing {
    private Menu M;
    private HashMap<Order, ArrayList<MenuItemWrapper>> orderList;

    public Restaurant() {
        orderList = new HashMap<>();
        try {
            M = loadMenu();
        } catch (Exception e) {
            System.out.println("MENU COULD NOT BE LOADED.");
            M = new Menu();
        }
    }

    public void saveMenu() throws IOException {
        File f = new File("menu.sv");
        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(f));
        stream.writeObject(M);
    }

    private Menu loadMenu() throws IOException, ClassNotFoundException {
        ObjectInputStream stream = new ObjectInputStream(new FileInputStream("menu.sv"));
        return (Menu) stream.readObject();
    }

    public ArrayList<MenuItem> getMenu() { return M.menu; }

    public void addItem(MenuItem I) {
        int prev = M.menu.size();
        M.menu.add(I);
        int post = M.menu.size();
        assert prev == post-1 : "Insertion failed";
    }
    public void updateItem(MenuItem oldVal, MenuItem newVal) {
        assert oldVal.equals(newVal) : "Updated value is the same as the previous value";
        Collections.replaceAll(M.menu, oldVal, newVal);
    }
    public void removeItems(List<MenuItem> items) {
        int prev = M.menu.size();
        for (MenuItem I : items) {
            M.menu.remove(I);
        }
        int post = M.menu.size();
        assert prev != post : "Deletion failed";
    }

    public void newOrder(List<MenuItemWrapper> items) throws EmptyOrderException {
        try {
            Order O = new Order(items);
            System.out.println(O);
            orderList.put(O, O.getContentList());
            setChanged();
            notifyObservers();
        } catch (EmptyOrderException e) {
            throw new EmptyOrderException();
        }
    }

    public ArrayList<Order> getOrders() {
//        ArrayList<Order> O = new ArrayList<>();
//        for(ArrayList<MenuItemWrapper> items : orderList.values()) {
//            try {
//                O.add(new Order(items));
//            } catch (EmptyOrderException e) {
//                e.printStackTrace();
//            }
//        }
//        return O;

        return new ArrayList(orderList.keySet());
    }
}
