package model;

import com.google.java.contract.Requires;
import menu.composite.MenuItem;

import java.util.List;

public interface RestaurantProcessing {


    @Requires("I != null")
    void addItem(MenuItem I);

    @Requires("newVal != null && oldVal != null && oldval != newVal")
    void updateItem(MenuItem oldVal, MenuItem newVal);

    @Requires("!items.isEmpty()")
    void removeItems(List<MenuItem> items);
}
