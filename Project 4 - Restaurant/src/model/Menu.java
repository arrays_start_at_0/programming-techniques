package model;

import menu.composite.MenuItem;

import java.io.*;
import java.util.ArrayList;

public class Menu implements Serializable {
    ArrayList<MenuItem> menu;

    Menu() {
        menu = new ArrayList<>();
    }

    public String toString () {
        String s = "";
        for(MenuItem I : menu) {
            s += I + "\n";
        }
        return s;
    }
}
