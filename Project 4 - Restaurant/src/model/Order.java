package model;

import exceptions.EmptyOrderException;
import menu.MenuItemWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Order {
    private ArrayList<MenuItemWrapper> contents;
    private int orderID;
    private double total = 0;
    private static int currentID = 1;

    Order(List<MenuItemWrapper> items) throws EmptyOrderException {
        contents = new ArrayList<>();
        for (MenuItemWrapper I : items) {
            if (I.getAmmount() > 0) {
                total += I.getPrice() * I.getAmmount();
                contents.add(I);
            }
        }
            if (contents.isEmpty()) {
            throw new EmptyOrderException();
        } else {
            orderID = currentID++;
        }
    }

    public int getID() { return orderID; }

    public String getContents() {
        String s = "";
        for(MenuItemWrapper I : contents) {
            s += I.getName();
            if(I.getAmmount() > 1) {
                s += " x" + I.getAmmount();
            }
            s += ", ";
        }
        return s.substring(0, s.length() - 2);
    }


    public double getTotal() { return total; }
    public ArrayList<MenuItemWrapper> getContentList() { return contents; }

    @Override
    public String toString() {
        return "Order no." + orderID + "\n" + getContents() + "\n" + "Total: " + getTotal();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderID == order.orderID &&
                Double.compare(order.total, total) == 0 &&
                Objects.equals(contents, order.contents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID);
    }
}
