package presentation;

import business_logic.OrderLogic;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Orders;

import java.util.ArrayList;
import java.util.Optional;

public class OrderMenu extends Application {

    private OrderLogic orderLogic;
    private ArrayList<Orders> orderList;

    private TableView<Orders> table;
    private TextField pageNumber;

    Stage stage;

    private final int WIDTH = 800;
    private final int HEIGHT = 410;
    private int startIndex = 0;

    @Override
    public void start(Stage orderStage) {
        orderLogic = new OrderLogic();
        orderList = new ArrayList<>();
        orderList = orderLogic.listAll();

        Button backButton = new Button("←");
        backButton.setOnAction(e -> Controller.launchMenu());
        //---------------------------------------------------------
        Button insertButton = new Button("+");
        insertButton.setOnAction(e -> Controller.launchNewOrder());
        Pane spacer1 = new Pane();
        HBox.setHgrow(spacer1, Priority.ALWAYS);
        Button prevPage = new Button("←");
        pageNumber = new TextField("0");
        pageNumber.setMinWidth(30);
        pageNumber.setMaxWidth(30);
        pageNumber.setEditable(false);
        Button nextPage = new Button("→");

        prevPage.setOnAction(e -> changeStartIndex(true));
        nextPage.setOnAction(e -> changeStartIndex(false));

        HBox controlsLayout = new HBox(1);
        controlsLayout.setAlignment(Pos.CENTER_RIGHT);
        controlsLayout.getChildren().addAll(insertButton, spacer1, prevPage, pageNumber, nextPage);
        //---------------------------------------------------------
        table = new TableView<>();

        ContextMenu rowMenu = new ContextMenu();
        MenuItem deleteItem = new MenuItem("Delete");
        deleteItem.setOnAction(e -> deleteCurrentClient());
        rowMenu.getItems().addAll(deleteItem);
        table.setOnContextMenuRequested(event -> rowMenu.show(table, event.getScreenX(), event.getScreenY()));

        TableColumn<Orders, Integer> ID = new TableColumn<>("ID");
        TableColumn<Orders, Integer> Client_ID = new TableColumn<>("Client ID");
        TableColumn<Orders, Integer> Product_ID = new TableColumn<>("Product ID");
        TableColumn<Orders, Integer> Quantity = new TableColumn<>("Quantity");
        TableColumn<Orders, Double> Price = new TableColumn<>("Price");

        ID.setCellValueFactory(new PropertyValueFactory<>("ID"));
        Client_ID.setCellValueFactory(new PropertyValueFactory<>("Client_ID"));
        Product_ID.setCellValueFactory(new PropertyValueFactory<>("Product_ID"));
        Quantity.setCellValueFactory(new PropertyValueFactory<>("Quantity"));
        Price.setCellValueFactory(new PropertyValueFactory<>("Price"));

        table.getColumns().addAll(ID, Client_ID, Product_ID, Quantity, Price);
        table.setMaxHeight(325);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        populateTable();
        //---------------------------------------------------------
        Separator separator = new Separator();

        VBox mainLayout = new VBox(5);
        mainLayout.getChildren().addAll(backButton, controlsLayout, separator, table);
        mainLayout.setPadding(new Insets(0, 5, 0, 5));
        Scene orderMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        orderStage.setScene(orderMenu);
        orderStage.setTitle("Current Orders");
        orderStage.show();
        stage = orderStage;
    }

    private void changeStartIndex(boolean back) {
        if (back) {
            if (startIndex - 10 >= 0) startIndex -= 10;
        } else {
            if (startIndex + 10 <= orderList.size() - 1) startIndex += 10;
        }
        populateTable();
    }

    private void populateTable() {
        table.getItems().clear();
        while(startIndex > 0 && startIndex >= orderList.size()) startIndex -= 10;
        pageNumber.setText(String.valueOf(startIndex/10 + 1));
        for (int i = startIndex; i < orderList.size() && i < startIndex + 10; i++) {
            table.getItems().add(orderList.get(i));
        }

        table.getColumns().get(0).setVisible(false);
        table.getColumns().get(0).setVisible(true);

    }

    private void deleteCurrentClient() {
        Orders O = table.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Deletion Process");
        alert.setHeaderText("Are you sure you want to delete the following order?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            orderLogic.deleteById(O.getID());
        }
        orderList = orderLogic.listAll();
        populateTable();
    }
}
