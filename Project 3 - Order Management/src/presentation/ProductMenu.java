package presentation;

import business_logic.ProductLogic;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Product;

import java.util.ArrayList;
import java.util.Optional;
import java.util.PriorityQueue;

public class ProductMenu extends Application {

    private ProductLogic productLogic;
    private ArrayList<Product> productList;

    private TableView<Product> table;
    private TextField searchBox;
    private Label advancedLabel;
    private Button advancedButton;
    private VBox advancedLayout;
    private TextField pageNumber;
    private ComboBox<String> orderType;
    private CheckBox orderDirection;

    private TextField NameField;
    private TextField BrandField;
    private TextField PriceField;
    private TextField QuantityField;

    Stage stage;

    private final int WIDTH = 800;
    private final int HEIGHT = 410;
    private int startIndex = 0;

    @Override
    public void start(Stage productStage) {
        productLogic = new ProductLogic();
        productList = new ArrayList<>();

        Button backButton = new Button("←");
        backButton.setOnAction(e -> Controller.launchMenu());
        //---------------------------------------------------------
        Button insertButton = new Button("+");
        insertButton.setOnAction(e -> showAdvanced("Insert"));
        Button deleteButton = new Button("‒");
        deleteButton.setOnAction(e -> showAdvanced("Delete"));
        Pane spacer1 = new Pane();
        HBox.setHgrow(spacer1, Priority.ALWAYS);
        orderType = new ComboBox<>();
        orderType.getItems().addAll("ID", "Name", "Brand", "Price", "Quantity");
        orderType.getSelectionModel().selectFirst();
        orderDirection = new CheckBox();
        Text searchText = new Text("Product name: ");
        searchBox = new TextField();
        Button searchButton = new Button("\uD83D\uDD0D");
        ContextMenu searchMenu = new ContextMenu();
        MenuItem advancedItem = new MenuItem("Advanced search");
        searchMenu.getItems().addAll(advancedItem);
        Button prevPage = new Button("←");
        pageNumber = new TextField("0");
        pageNumber.setMinWidth(30);
        pageNumber.setMaxWidth(30);
        pageNumber.setEditable(false);
        Button nextPage = new Button("→");

        searchButton.setOnContextMenuRequested(event -> searchMenu.show(searchButton, event.getScreenX(), event.getScreenY()));
        searchButton.setOnAction(e -> search());
        advancedItem.setOnAction(e -> showAdvanced("Search"));
        prevPage.setOnAction(e -> changeStartIndex(true));
        nextPage.setOnAction(e -> changeStartIndex(false));

        HBox controlsLayout = new HBox(1);
        controlsLayout.setAlignment(Pos.CENTER_RIGHT);
        controlsLayout.getChildren().addAll(insertButton, deleteButton, new Text(" Order by: "), orderType, new Text(" Desc: "), orderDirection, spacer1, searchText, searchBox, searchButton, prevPage, pageNumber, nextPage);

        productList = productLogic.listAll(orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
        //---------------------------------------------------------
        advancedLabel = new Label("Advanced Search");
        advancedLabel.setFont(new Font(30));
        HBox label = new HBox(advancedLabel);
        label.setAlignment(Pos.CENTER_LEFT);
        Text NameText = new Text("Product Name:        ");
        Text BrandText = new Text("Brand Name:        ");
        Text PriceText = new Text("Price:                       ");
        Text QuantityText = new Text("Quantity:              ");

        NameField = new TextField();
        BrandField = new TextField();
        PriceField = new TextField();
        QuantityField = new TextField();

        VBox adv1 = new VBox(new HBox(NameText, NameField), new HBox(PriceText, PriceField));
        VBox adv2 = new VBox(new HBox(BrandText, BrandField), new HBox(QuantityText, QuantityField));

        Pane spacer2 = new Pane();
        HBox.setHgrow(spacer2, Priority.ALWAYS);

        advancedButton = new Button("Search");
        advancedButton.setOnAction(e -> advancedAction());

        advancedLayout = new VBox(5);
        advancedLayout.getChildren().addAll(label, new HBox(adv1, spacer2, adv2), advancedButton);
        advancedLayout.setAlignment(Pos.CENTER);
        advancedLayout.setVisible(false);
        advancedLayout.setManaged(false);
        //---------------------------------------------------------
        table = new TableView<>();

        ContextMenu rowMenu = new ContextMenu();
        MenuItem updateItem = new MenuItem("Update");
        MenuItem saveItem = new MenuItem("Save");
        MenuItem deleteItem = new MenuItem("Delete");
        deleteItem.setOnAction(e -> deleteCurrentClient());
        saveItem.setOnAction(e -> saveProduct());
        updateItem.setOnAction(e -> showAdvanced("Update"));
        rowMenu.getItems().addAll(updateItem, saveItem, deleteItem);
        table.setOnContextMenuRequested(event -> rowMenu.show(table, event.getScreenX(), event.getScreenY()));

        TableColumn<Product, Integer> ID = new TableColumn<>("ID");
        TableColumn<Product, String> Name = new TableColumn<>("Product Name");
        TableColumn<Product, String> Brand = new TableColumn<>("Brand Name");
        TableColumn<Product, Double> Price = new TableColumn<>("Price");
        TableColumn<Product, Integer> Quantity = new TableColumn<>("Quantity");

        ID.setCellValueFactory(new PropertyValueFactory<>("ID"));
        Name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        Brand.setCellValueFactory(new PropertyValueFactory<>("Brand"));
        Price.setCellValueFactory(new PropertyValueFactory<>("Price"));
        Quantity.setCellValueFactory(new PropertyValueFactory<>("Quantity"));

        table.getColumns().addAll(ID, Name, Brand, Price, Quantity);
        table.setMaxHeight(325);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        populateTable();
        //---------------------------------------------------------
        Separator separator = new Separator();

        VBox mainLayout = new VBox(5);
        mainLayout.getChildren().addAll(backButton, controlsLayout, separator, advancedLayout, table);
        mainLayout.setPadding(new Insets(0, 5, 0, 5));
        Scene productMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        productStage.setScene(productMenu);
        productStage.setTitle("Product Operations");
        productStage.show();
        stage = productStage;
    }

    private void saveProduct() {
        Product P = table.getSelectionModel().getSelectedItem();
        if(Controller.saveProduct(P)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success!");
            alert.setHeaderText("Product has been saved.");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Product already memorised.");
            alert.show();
        }
    }

    private void changeStartIndex(boolean back) {
        if (back) {
            if (startIndex - 10 >= 0) startIndex -= 10;
        } else {
            if (startIndex + 10 <= productList.size() - 1) startIndex += 10;
        }
        populateTable();
    }

    private void populateTable() {
        table.getItems().clear();
        while(startIndex > 0 && startIndex >= productList.size()) startIndex -= 10;
        pageNumber.setText(String.valueOf(startIndex/10 + 1));
        for (int i = startIndex; i < productList.size() && i < startIndex + 10; i++) {
            table.getItems().add(productList.get(i));
        }

        table.getColumns().get(0).setVisible(false);
        table.getColumns().get(0).setVisible(true);

    }

    private void search() {
        if (!searchBox.getText().isEmpty()) {
            String[] parameters = new String[4];
            parameters[0] = searchBox.getText();
            productList = productLogic.find(parameters, orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
        } else {
            productList = productLogic.listAll(orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
        }
        populateTable();
    }

    private void advancedSeach() {
        String[] parameters = getParam();
        if(isValidInput()) {
            productList = productLogic.find(parameters, orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
            populateTable();
        }

    }

    private void insert() {
        String[] parameters = getParam();

        if(isValidInput())
            try {
                productLogic.insert(parameters);
                productList = productLogic.listAll(orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
                populateTable();
            } catch (IllegalArgumentException e) {
                Alert error = new Alert(Alert.AlertType.ERROR);
                error.setTitle("Invalid input");
                error.setHeaderText(e.getLocalizedMessage());
                error.show();
            }
    }

    private void deleteCurrentClient() {
        Product P = table.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Deletion Process");
        alert.setHeaderText("Are you sure you want to delete the following client?");
        alert.setContentText(P.getID() + ", " + P.getName() + " " + P.getBrand());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Controller.deleteProduct(P);
            productLogic.deleteById(P.getID());
        }
        productList = productLogic.listAll(orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
        populateTable();
    }

    private void delete() {
        String[] parameters = getParam();

        if(isValidInput())
        {
            int deleteCount = productLogic.delete(parameters);
            if(deleteCount == 0) {
                Alert deleteError = new Alert(Alert.AlertType.ERROR);
                deleteError.setTitle("Deletion Unsuccessful");
                deleteError.setHeaderText("No elements were deleted.");
                deleteError.show();
            } else {
                Alert deleteConfirm = new Alert(Alert.AlertType.INFORMATION);
                deleteConfirm.setTitle("Deletion Successful");
                deleteConfirm.setHeaderText("Rows deleted: " + deleteCount);
                deleteConfirm.show();
            }
            productList = productLogic.listAll(orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
            populateTable();
        }
    }

    private void update() {
        Product P = table.getSelectionModel().getSelectedItem();
        if(P != null) {
            String[] parameters = getParam();
            if(isValidInput()) {
                try {
                    productLogic.update(parameters, P.getID());
                    productList = productLogic.listAll(orderType.getSelectionModel().getSelectedItem(), orderDirection.isSelected());
                    populateTable();
                } catch (IllegalArgumentException e) {
                    Alert error = new Alert(Alert.AlertType.ERROR);
                    error.setTitle("Invalid input");
                    error.setHeaderText(e.getLocalizedMessage());
                    error.show();
                }
            }
        }
    }

    private String[] getParam() {
        String[] parameters = new String[4];
        parameters[0] = NameField.getText();
        parameters[1] = BrandField.getText();
        parameters[2] = PriceField.getText();
        parameters[3] = QuantityField.getText();

        return parameters;
    }

    private boolean isValidInput() {
        if(PriceField.getText().isEmpty()) return false;
        if(QuantityField.getText().isEmpty()) return false;

        if(!NameField.getText().isEmpty()) return true;
        if(!BrandField.getText().isEmpty()) return true;

        return false;
    }

    private void advancedAction() {
        switch (advancedButton.getText()) {
            case "Search":
                advancedSeach();
                break;
            case "Insert":
                insert();
                closeAdvanced();
                break;
            case "Delete":
                delete();
                closeAdvanced();
                break;
            case "Update":
                update();
                closeAdvanced();
                break;
        }
    }

    private void closeAdvanced() {
        advancedLayout.setVisible(false);
        advancedLayout.setManaged(false);

        //stage.setHeight(HEIGHT);
        stage.sizeToScene();
    }

    private void showAdvanced(String mode) {
        advancedLayout.setVisible(true);
        advancedLayout.setManaged(true);

        switch(mode) {
            case "Search" :
                advancedLabel.setText("Advanced Search:");
                advancedButton.setText("Search");
                break;
            case "Insert" :
                advancedLabel.setText("Insert new Product:");
                advancedButton.setText("Insert");
                break;
            case "Update" :
                advancedLabel.setText("Update selected Products:");
                advancedButton.setText("Update");
                break;
            case "Delete" :
                advancedLabel.setText("Delete Products");
                advancedButton.setText("Delete");
                break;
        }
        stage.setHeight(HEIGHT + 230);
    }
}
