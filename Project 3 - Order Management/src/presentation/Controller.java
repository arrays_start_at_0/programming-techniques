	package presentation;

import javafx.application.Application;
import javafx.stage.Stage;
import model.Client;
import model.Product;

import java.util.ArrayList;
import java.util.HashSet;

public class Controller extends Application {

    private static Stage mainStage;
    private static MainMenu menu;

    private static ClientMenu clientMenu;
    private static ProductMenu productMenu;
    private static OrderMenu orderMenu;
    private static NewOrderMenu newOrderMenu;
    private static QuantityMenu quantityMenu;

    private static HashSet<Client> savedClients;
    private static HashSet<Product> savedProducts;

    public static void main(String[] args)  {
        savedClients = new HashSet<>();
        savedProducts = new HashSet<>();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage){
        mainStage = new Stage();
        launchMenu();
    }

    static void launchClient() {
        if(clientMenu == null) {
            clientMenu = new ClientMenu();
        }
        clientMenu.start(mainStage);
    }

    static void launchProduct() {
        if(productMenu == null) {
            productMenu = new ProductMenu();
        }
        productMenu.start(mainStage);
    }

    static void launchOrder() {
        if(orderMenu == null) {
            orderMenu = new OrderMenu();
        }
        orderMenu.start(mainStage);
    }

    static void launchNewOrder() {
        if(newOrderMenu == null) {
            newOrderMenu = new NewOrderMenu();
        }
        newOrderMenu.start(mainStage);
    }

    static void nextOrdePage(Client C, ArrayList<Product> selectedProducts) {
        quantityMenu = new QuantityMenu(C, selectedProducts);
        quantityMenu.start(mainStage);
    }


    static void launchMenu() {
        if(menu == null) {
            menu = new MainMenu();
        }
        menu.start(mainStage);
    }

    static boolean saveClient(Client C) {
        if(!savedClients.contains(C)) {
            savedClients.add(C);
            return true;
        }
        return false;
    }

    static boolean saveProduct(Product P) {
        if(!savedProducts.contains(P)) {
            savedProducts.add(P);
            return true;
        }
        return false;
    }

    static void deleteClient(Client C) {
        savedClients.remove(C);
    }

    static void deleteProduct(Product P) {
        savedProducts.remove(P);
    }

    public static HashSet<Client> getSavedClients() {
        return savedClients;
    }

    public static HashSet<Product> getSavedProducts() {
        return savedProducts;
    }
}
