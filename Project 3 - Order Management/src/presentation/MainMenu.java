package presentation;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.Window;


public class MainMenu extends Application {
    private final int WIDTH = 400;
    private final int HEIGHT = 400;

    @Override
    public void start(Stage menuStage) {
        Button clientButton = new Button("Client Operations");
        clientButton.setFont(new Font(30));
        clientButton.setPrefHeight(Integer.MAX_VALUE);
        clientButton.setPrefWidth(Integer.MAX_VALUE);
        clientButton.setOnAction(e -> Controller.launchClient());

        Button productButton = new Button("Product Operations");
        productButton.setFont(new Font(30));
        productButton.setPrefHeight(Integer.MAX_VALUE);
        productButton.setPrefWidth(Integer.MAX_VALUE);
        productButton.setOnAction(e -> Controller.launchProduct());

        Button orderButton = new Button("Current Orders");
        orderButton.setFont(new Font(30));
        orderButton.setPrefHeight(Integer.MAX_VALUE);
        orderButton.setPrefWidth(Integer.MAX_VALUE);
        orderButton.setOnAction(e -> Controller.launchOrder());

        Button newOrderButton = new Button("Make an Order");
        newOrderButton.setFont(new Font(30));
        newOrderButton.setPrefHeight(Integer.MAX_VALUE);
        newOrderButton.setPrefWidth(Integer.MAX_VALUE);
        newOrderButton.setOnAction(e -> Controller.launchNewOrder());

        VBox mainLayout = new VBox(20);
        mainLayout.setPadding(new Insets(10));
        mainLayout.getChildren().addAll(clientButton, productButton, orderButton, newOrderButton);
        Scene mainMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        menuStage.setScene(mainMenu);
        menuStage.setTitle("Main Menu");
        menuStage.show();
    }
}
