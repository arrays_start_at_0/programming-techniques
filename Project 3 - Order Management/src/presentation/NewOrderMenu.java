package presentation;

import business_logic.ClientLogic;
import business_logic.ProductLogic;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Client;
import model.Product;

import java.util.ArrayList;

public class NewOrderMenu extends Application {
    private final int WIDTH = 800;
    private final int HEIGHT = 800;

    private ClientLogic clientLogic;
    private ProductLogic productLogic;

    private ArrayList<Client> savedClients;
    private ArrayList<Product> savedProducts;

    private TableView<Client> clientTable;
    private TableView<Product> productTable;

    @Override
    public void start(Stage newOrderStage) {
        clientLogic = new ClientLogic();
        productLogic = new ProductLogic();

        savedClients = new ArrayList<>(Controller.getSavedClients());
        savedProducts = new ArrayList<>(Controller.getSavedProducts());

        //----------------------------------------------------------------------------------------
        Button backButton = new Button("←");
        backButton.setOnAction(e -> Controller.launchMenu());
        //----------------------------------------------------------------------------------------

        Label cLabel = new Label("Select a client: ");
        clientTable = new TableView<>();

        ContextMenu rowClient = new ContextMenu();
        MenuItem deleteClient = new MenuItem("Delete");
        deleteClient.setOnAction(e -> deleteItem(savedClients, clientTable));
        rowClient.getItems().addAll(deleteClient);
        clientTable.setOnContextMenuRequested(event -> rowClient.show(clientTable, event.getScreenX(), event.getScreenY()));

        TableColumn<Client, Integer> ID = new TableColumn<>("ID");
        TableColumn<Client, String> FirstName = new TableColumn<>("First Name");
        TableColumn<Client, String> LastName = new TableColumn<>("LastName");
        TableColumn<Client, String> CNP = new TableColumn<>("CNP");
        TableColumn<Client, String> Phone = new TableColumn<>("Phone Number");
        TableColumn<Client, String> Email = new TableColumn<>("Email");
        TableColumn<Client, String> Address = new TableColumn<>("Address");

        ID.setCellValueFactory(new PropertyValueFactory<>("ID"));
        FirstName.setCellValueFactory(new PropertyValueFactory<>("FirstName"));
        LastName.setCellValueFactory(new PropertyValueFactory<>("LastName"));
        CNP.setCellValueFactory(new PropertyValueFactory<>("CNP"));
        Phone.setCellValueFactory(new PropertyValueFactory<>("Phone"));
        Email.setCellValueFactory(new PropertyValueFactory<>("Email"));
        Address.setCellValueFactory(new PropertyValueFactory<>("Address"));

        clientTable.getColumns().addAll(ID, FirstName, LastName, CNP, Phone, Email, Address);
        clientTable.setMaxHeight(325);
        clientTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //----------------------------------------------------------------------------------------
        Separator separator = new Separator();
        //----------------------------------------------------------------------------------------

        Label pLabel = new Label("Select one or more products: ");
        productTable = new TableView<>();
        ContextMenu rowProduct = new ContextMenu();
        MenuItem deleteProduct = new MenuItem("Delete");
        deleteProduct.setOnAction(e -> deleteItem(savedProducts, productTable));
        rowProduct.getItems().addAll(deleteProduct);
        productTable.setOnContextMenuRequested(event -> rowProduct.show(productTable, event.getScreenX(), event.getScreenY()));

        TableColumn<Product, Integer> P_ID = new TableColumn<>("ID");
        TableColumn<Product, String> Name = new TableColumn<>("Product Name");
        TableColumn<Product, String> Brand = new TableColumn<>("Brand Name");
        TableColumn<Product, Double> Price = new TableColumn<>("Price");
        TableColumn<Product, Integer> Inventory = new TableColumn<>("Inventory");

        P_ID.setCellValueFactory(new PropertyValueFactory<>("ID"));
        Name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        Brand.setCellValueFactory(new PropertyValueFactory<>("Brand"));
        Price.setCellValueFactory(new PropertyValueFactory<>("Price"));
        Inventory.setCellValueFactory(new PropertyValueFactory<>("Quantity"));

        productTable.getColumns().addAll(P_ID, Name, Brand, Price, Inventory);
        productTable.setMaxHeight(325);
        productTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        productTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //----------------------------------------------------------------------------------------
        populateTables();

        Button nextPage = new Button("Next");
        nextPage.setOnAction(e -> nextPage());
        HBox btnLayout = new HBox(nextPage);
        btnLayout.setAlignment(Pos.CENTER);

        VBox mainLayout = new VBox(5);
        mainLayout.getChildren().addAll(backButton, cLabel, clientTable, separator, pLabel, productTable, btnLayout);
        mainLayout.setPadding(new Insets(5));
        Scene clientMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        newOrderStage.setScene(clientMenu);
        newOrderStage.setTitle("New Order");
        newOrderStage.show();
    }

    private void deleteItem(ArrayList list, TableView table) {
        Object O = table.getSelectionModel().getSelectedItem();
        list.remove(O);
        if(O.getClass().getSimpleName().equals("Client")) {
            Controller.deleteClient((Client)O);
        } else {
            Controller.deleteProduct((Product)O);
        }
        populateTables();
    }

    private void populateTables() {
        clientTable.getItems().clear();
        productTable.getItems().clear();

        for (int i = 0; i < savedClients.size(); i++) {
            if (clientLogic.findById(savedClients.get(i).getID()) != null) {
                clientTable.getItems().add(savedClients.get(i));
            } else {
                Controller.deleteClient(savedClients.get(i));
                savedClients.remove(i);
                i--;
            }
        }

        for (int i = 0; i < savedProducts.size(); i++) {
            if (productLogic.findById(savedProducts.get(i).getID()) != null) {
                productTable.getItems().add(savedProducts.get(i));
            } else {
                Controller.deleteProduct(savedProducts.get(i));
                savedProducts.remove(i);
                i--;
            }
        }
    }

    private void nextPage() {
        Client C = clientTable.getSelectionModel().getSelectedItem();
        ArrayList<Product> P = new ArrayList<>(productTable.getSelectionModel().getSelectedItems());
        if(C != null && !P.isEmpty()) {
            Controller.nextOrdePage(C, P);
        }
    }

}
