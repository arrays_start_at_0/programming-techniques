package presentation;

import business_logic.ClientLogic;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Client;

import java.util.ArrayList;
import java.util.Optional;

public class ClientMenu extends Application {

    private ClientLogic clientLogic;
    private ArrayList<Client> clientList;

    private TableView<Client> table;
    private TextField searchBox;
    private Label advancedLabel;
    private Button advancedButton;
    private VBox advancedLayout;
    private TextField pageNumber;

    private TextField FNameField;
    private TextField LNameField;
    private TextField CNPField;
    private TextField PhoneField;
    private TextField EmailField;
    private TextField AddressField;

    Stage stage;

    private final int WIDTH = 800;
    private final int HEIGHT = 410;
    private int startIndex = 0;

    @Override
    public void start(Stage clientStage) {
        clientLogic = new ClientLogic();
        clientList = new ArrayList<>();
        clientList = clientLogic.listAll();

        Button backButton = new Button("←");
        backButton.setOnAction(e -> Controller.launchMenu());
        //---------------------------------------------------------
        Button insertButton = new Button("+");
        insertButton.setOnAction(e -> showAdvanced("Insert"));
        Button deleteButton = new Button("‒");
        deleteButton.setOnAction(e -> showAdvanced("Delete"));
        Pane spacer1 = new Pane();
        HBox.setHgrow(spacer1, Priority.ALWAYS);
        Text searchText = new Text("Client name: ");
        searchBox = new TextField();
        Button searchButton = new Button("\uD83D\uDD0D");
        ContextMenu searchMenu = new ContextMenu();
        MenuItem advancedItem = new MenuItem("Advanced search");
        searchMenu.getItems().addAll(advancedItem);
        Button prevPage = new Button("←");
        pageNumber = new TextField("0");
        pageNumber.setMinWidth(30);
        pageNumber.setMaxWidth(30);
        pageNumber.setEditable(false);
        Button nextPage = new Button("→");

        searchButton.setOnContextMenuRequested(event -> searchMenu.show(searchButton, event.getScreenX(), event.getScreenY()));
        searchButton.setOnAction(e -> search());
        advancedItem.setOnAction(e -> showAdvanced("Search"));
        prevPage.setOnAction(e -> changeStartIndex(true));
        nextPage.setOnAction(e -> changeStartIndex(false));

        HBox controlsLayout = new HBox(1);
        controlsLayout.setAlignment(Pos.CENTER_RIGHT);
        controlsLayout.getChildren().addAll(insertButton, deleteButton, spacer1, searchText, searchBox, searchButton, prevPage, pageNumber, nextPage);
        //---------------------------------------------------------
        advancedLabel = new Label("Advanced Search");
        advancedLabel.setFont(new Font(30));
        HBox label = new HBox(advancedLabel);
        label.setAlignment(Pos.CENTER_LEFT);
        Text FNameText = new Text("First Name:        ");
        Text LNameText = new Text("Last Name:        ");
        Text CNPText = new Text("CNP:                  ");
        Text PhoneText = new Text("Phone Number: ");
        Text EmailText = new Text("Email Address:  ");
        Text AddressText = new Text("Address:            ");

        FNameField = new TextField();
        LNameField = new TextField();
        CNPField = new TextField();
        PhoneField = new TextField();
        EmailField = new TextField();
        AddressField = new TextField();

        VBox adv1 = new VBox(new HBox(FNameText, FNameField), new HBox(CNPText, CNPField), new HBox(PhoneText, PhoneField));
        VBox adv2 = new VBox(new HBox(LNameText, LNameField), new HBox(EmailText, EmailField), new HBox(AddressText, AddressField));

        Pane spacer2 = new Pane();
        HBox.setHgrow(spacer2, Priority.ALWAYS);

        advancedButton = new Button("Search");
        advancedButton.setOnAction(e -> advancedAction());

        advancedLayout = new VBox(5);
        advancedLayout.getChildren().addAll(label, new HBox(adv1, spacer2, adv2), advancedButton);
        advancedLayout.setAlignment(Pos.CENTER);
        advancedLayout.setVisible(false);
        advancedLayout.setManaged(false);
        //---------------------------------------------------------
        table = new TableView<>();

        ContextMenu rowMenu = new ContextMenu();
        MenuItem updateItem = new MenuItem("Update");
        MenuItem saveItem = new MenuItem("Save");
        MenuItem deleteItem = new MenuItem("Delete");
        deleteItem.setOnAction(e -> deleteCurrentClient());
        saveItem.setOnAction(e -> saveClient());
        updateItem.setOnAction(e -> showAdvanced("Update"));
        rowMenu.getItems().addAll(updateItem, saveItem, deleteItem);
        table.setOnContextMenuRequested(event -> rowMenu.show(table, event.getScreenX(), event.getScreenY()));

        TableColumn<Client, Integer> ID = new TableColumn<>("ID");
        TableColumn<Client, String> FirstName = new TableColumn<>("First Name");
        TableColumn<Client, String> LastName = new TableColumn<>("LastName");
        TableColumn<Client, String> CNP = new TableColumn<>("CNP");
        TableColumn<Client, String> Phone = new TableColumn<>("Phone Number");
        TableColumn<Client, String> Email = new TableColumn<>("Email");
        TableColumn<Client, String> Address = new TableColumn<>("Address");

        ID.setCellValueFactory(new PropertyValueFactory<>("ID"));
        FirstName.setCellValueFactory(new PropertyValueFactory<>("FirstName"));
        LastName.setCellValueFactory(new PropertyValueFactory<>("LastName"));
        CNP.setCellValueFactory(new PropertyValueFactory<>("CNP"));
        Phone.setCellValueFactory(new PropertyValueFactory<>("Phone"));
        Email.setCellValueFactory(new PropertyValueFactory<>("Email"));
        Address.setCellValueFactory(new PropertyValueFactory<>("Address"));

        table.getColumns().addAll(ID, FirstName, LastName, CNP, Phone, Email, Address);
        table.setMaxHeight(325);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        populateTable();
        //---------------------------------------------------------
        Separator separator = new Separator();

        VBox mainLayout = new VBox(5);
        mainLayout.getChildren().addAll(backButton, controlsLayout, separator, advancedLayout, table);
        mainLayout.setPadding(new Insets(0, 5, 0, 5));
        Scene clientMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        clientStage.setScene(clientMenu);
        clientStage.setTitle("Client Operations");
        clientStage.show();
        stage = clientStage;
    }

    private void saveClient() {
        Client C = table.getSelectionModel().getSelectedItem();
        if(Controller.saveClient(C)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Success!");
            alert.setHeaderText("Client has been saved.");
            alert.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Client already memorised.");
            alert.show();
        }
    }

    private void changeStartIndex(boolean back) {
        if (back) {
            if (startIndex - 10 >= 0) startIndex -= 10;
        } else {
            if (startIndex + 10 <= clientList.size() - 1) startIndex += 10;
        }
        populateTable();
    }

    private void populateTable() {
        table.getItems().clear();
        while(startIndex > 0 && startIndex >= clientList.size()) startIndex -= 10;
        pageNumber.setText(String.valueOf(startIndex/10 + 1));
        for (int i = startIndex; i < clientList.size() && i < startIndex + 10; i++) {
            table.getItems().add(clientList.get(i));
        }

        table.getColumns().get(0).setVisible(false);
        table.getColumns().get(0).setVisible(true);

    }

    private void search() {
        if (!searchBox.getText().isEmpty()) {
            String[] parameters = new String[6];
            String[] fullName = searchBox.getText().split(" ");
            System.arraycopy(fullName, 0, parameters, 0, fullName.length);

            clientList = clientLogic.find(parameters);
        } else {
            clientList = clientLogic.listAll();
        }
        populateTable();
    }

    private void advancedSeach() {
        String[] parameters = getParam();
        if(isValidInput()) {
            clientList = clientLogic.find(parameters);
            populateTable();
        }

    }

    private void insert() {
        String[] parameters = getParam();

        if(isValidInput())
        try {
            clientLogic.insert(parameters);
            clientList = clientLogic.listAll();
            populateTable();
        } catch (IllegalArgumentException e) {
            Alert error = new Alert(Alert.AlertType.ERROR);
            error.setTitle("Invalid input");
            error.setHeaderText(e.getLocalizedMessage());
            error.show();
        }
    }

    private void deleteCurrentClient() {
        Client C = table.getSelectionModel().getSelectedItem();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Deletion Process");
        alert.setHeaderText("Are you sure you want to delete the following client?");
        alert.setContentText(C.getID() + ", " + C.getFirstName() + " " + C.getLastName());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Controller.deleteClient(C);
            clientLogic.deleteById(C.getID());
        }
        clientList = clientLogic.listAll();
        populateTable();
    }

    private void delete() {
        String[] parameters = getParam();

        if(isValidInput())
        {
            int deleteCount = clientLogic.delete(parameters);
            if(deleteCount == 0) {
                Alert deleteError = new Alert(Alert.AlertType.ERROR);
                deleteError.setTitle("Deletion Unsuccessful");
                deleteError.setHeaderText("No elements were deleted.");
                deleteError.show();
            } else {
                Alert deleteConfirm = new Alert(Alert.AlertType.INFORMATION);
                deleteConfirm.setTitle("Deletion Successful");
                deleteConfirm.setHeaderText("Rows deleted: " + deleteCount);
                deleteConfirm.show();
            }
            clientList = clientLogic.listAll();
            populateTable();
        }
    }

    private void update() {
        Client C = table.getSelectionModel().getSelectedItem();
        if(C != null) {
            String[] parameters = getParam();
            if(isValidInput()) {
                try {
                    clientLogic.update(parameters, C.getID());
                    clientList = clientLogic.listAll();
                    populateTable();
                } catch (IllegalArgumentException e) {
                    Alert error = new Alert(Alert.AlertType.ERROR);
                    error.setTitle("Invalid input");
                    error.setHeaderText(e.getLocalizedMessage());
                    error.show();
                }
            }
        }
    }

    private String[] getParam() {
        String[] parameters = new String[6];
        parameters[0] = FNameField.getText();
        parameters[1] = LNameField.getText();
        parameters[2] = CNPField.getText();
        parameters[3] = PhoneField.getText();
        parameters[4] = EmailField.getText();
        parameters[5] = AddressField.getText();

        return parameters;
    }

    private boolean isValidInput() {
        if(!FNameField.getText().isEmpty()) return true;
        if(!LNameField.getText().isEmpty()) return true;
        if(!CNPField.getText().isEmpty()) return true;
        if(!PhoneField.getText().isEmpty()) return true;
        if(!EmailField.getText().isEmpty()) return true;
        if(!AddressField.getText().isEmpty()) return true;

        return false;
    }

    private void advancedAction() {
        switch (advancedButton.getText()) {
            case "Search":
                advancedSeach();
                break;
            case "Insert":
                insert();
                closeAdvanced();
                break;
            case "Delete":
                delete();
                closeAdvanced();
                break;
            case "Update":
                update();
                closeAdvanced();
                break;
        }
    }

    private void closeAdvanced() {
        advancedLayout.setVisible(false);
        advancedLayout.setManaged(false);

        //stage.setHeight(HEIGHT);
        stage.sizeToScene();
    }

    private void showAdvanced(String mode) {
        advancedLayout.setVisible(true);
        advancedLayout.setManaged(true);

        switch(mode) {
            case "Search" :
                advancedLabel.setText("Advanced Search:");
                advancedButton.setText("Search");
                break;
            case "Insert" :
                advancedLabel.setText("Insert new Client:");
                advancedButton.setText("Insert");
                break;
            case "Update" :
                advancedLabel.setText("Update selected Client:");
                advancedButton.setText("Update");
                break;
            case "Delete" :
                advancedLabel.setText("Delete Clients");
                advancedButton.setText("Delete");
                break;
        }

        stage.setHeight(HEIGHT + 230);
    }
}
