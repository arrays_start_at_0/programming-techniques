package presentation;

import business_logic.OrderLogic;
import business_logic.ProductLogic;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Client;
import model.Product;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class QuantityMenu extends Application {
    private OrderLogic orderLogic;
    private ProductLogic productLogic;

    private int WIDTH = 300;
    private int HEIGHT = 200;

    private Client C;
    private ArrayList<Product> selected;

    private ArrayList<Spinner<Integer>> spinners;
    private ArrayList<Text> pricetags;

    private Text finalPrice;
    private VBox quantityLayout;

    QuantityMenu(Client C, ArrayList<Product> selected) {
        this.C = C;
        this.selected = selected;
        spinners = new ArrayList<>();
        pricetags = new ArrayList<>();
        quantityLayout = new VBox(5);
        HEIGHT += 60 * selected.size();
        finalPrice = new Text();
        populateLayout();
        orderLogic = new OrderLogic();
        productLogic = new ProductLogic();
    }

    @Override
    public void start(Stage nextPage) {
        Button backButton = new Button("←");
        backButton.setOnAction(e -> Controller.launchNewOrder());
        //----------------------------------------------------------------------------------------
        Text orderText = new Text("Order for Client " + C.getFirstName() + " " + C.getLastName() + ": ");
        //----------------------------------------------------------------------------------------
        quantityLayout.setAlignment(Pos.CENTER);
        //----------------------------------------------------------------------------------------
        Button makeOrder = new Button("Make Order");
        makeOrder.setOnAction(e -> makeOrder());
        VBox finalLayout = new VBox(finalPrice, makeOrder);
        finalLayout.setAlignment(Pos.CENTER);
        //----------------------------------------------------------------------------------------
        VBox mainLayout = new VBox(10);
        mainLayout.getChildren().addAll(backButton, orderText, new Separator(), quantityLayout, new Separator(), finalLayout);
        mainLayout.setPadding(new Insets(5));
        Scene clientMenu = new Scene(mainLayout, WIDTH, HEIGHT);
        nextPage.setScene(clientMenu);
        nextPage.setTitle("New Order");
        nextPage.show();
    }

    private void populateLayout() {
        for (int i = 0; i < selected.size(); i++) {
            HBox row = new HBox(5);
            spinners.add(new Spinner<>(1, selected.get(i).getQuantity(), 1));
            spinners.get(i).setMaxWidth(100);
            spinners.get(i).setMinWidth(100);
            spinners.get(i).setEditable(true);
            final int index = i;
            spinners.get(i).valueProperty().addListener((obs, oldValue, newValue) -> changePrice(index));
            pricetags.add(new Text("Price = " + selected.get(i).getPrice() + "$"));

            row.getChildren().addAll(new Text(selected.get(i).getName() + ": "), spinners.get(i), pricetags.get(i));
            row.setAlignment(Pos.CENTER);
            quantityLayout.getChildren().add(row);
        }
        updateTotal();
    }

    private void changePrice(int index) {
        pricetags.get(index).setText("Price = " + (selected.get(index).getPrice() * spinners.get(index).getValue()) + "$");
        updateTotal();
    }

    private void updateTotal() {
        double total = 0;
        for (int i = 0; i < spinners.size(); i++) {
            total += (selected.get(i).getPrice() * spinners.get(i).getValue());
        }
        finalPrice.setText("Total price: " + total + "$");
    }

    private void makeOrder() {
        for(int i = 0; i < selected.size(); i++) {
            orderLogic.insert(C.getID(), selected.get(i).getID(), spinners.get(i).getValue(), (selected.get(i).getPrice() * spinners.get(i).getValue()));
            productLogic.update(new String[]{"", "", "", String.valueOf(selected.get(i).getQuantity() - spinners.get(i).getValue())}, selected.get(i).getID());
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());
        String fileName = "Bill " + date + ".txt";
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(("Date: " + date).getBytes());
            fos.write(("\n\nClient: " +
                        "\n\t" + C.getFirstName() + " " + C.getLastName() +
                        "\n\tCNP: " + C.getCNP()+
                        "\n\tPhone Number: " + C.getPhone() +
                        "\n\tEmail: " + C.getEmail() +
                        "\n\tLiving at address: " + C.getAddress() +
                        "\n-------------------------------------------------------------------------------------" +
                        "\n\nProducts: ").getBytes());
            for(int  i = 0; i < selected.size(); i++) {
                fos.write(("\n\t" + selected.get(i).getName() + " " + selected.get(i).getBrand() +
                            "\n\t\tQuantity: " + spinners.get(i).getValue() + "\n\t\tPrice per item: " + selected.get(i).getPrice() + "$" +
                            "\n\t\tTotal price: " + (selected.get(i).getPrice() * spinners.get(i).getValue()) + "$\n").getBytes());
            }
            fos.write("\n-------------------------------------------------------------------------------------".getBytes());
            fos.write(("\n\n" + finalPrice.getText()).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Alert confirmation = new Alert(Alert.AlertType.INFORMATION);
        confirmation.setTitle("Success!");
        confirmation.setHeaderText("Order generated successfully.");
        confirmation.setContentText("A receipt has been generated for your convenience.");
        confirmation.show();
        Controller.launchMenu();
    }
}
