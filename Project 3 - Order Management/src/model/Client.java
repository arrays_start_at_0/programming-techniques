package model;

public class Client {
    private int ID;
    private String FirstName;
    private String LastName;
    private String CNP;
    private String Phone;
    private String Email;
    private String Address;

    public Client() {
    }

    public Client(String[] paramList) {
        this.FirstName = paramList[0];
        this.LastName = paramList[1];
        this.CNP = paramList[2];
        this.Phone = paramList[3];
        this.Email = paramList[4];
        this.Address = paramList[5];
    }

    public int getID() { return ID; }
    public void setID(int ID) { this.ID = ID; }

    public String getFirstName() { return FirstName; }
    public void setFirstName(String firstName) { FirstName = firstName; }

    public String getLastName() { return LastName; }
    public void setLastName(String lastName) { LastName = lastName; }

    public String getCNP() { return CNP; }
    public void setCNP(String CNP) { this.CNP = CNP; }

    public String getPhone() { return Phone; }
    public void setPhone(String phone) { Phone = phone; }

    public String getEmail() { return Email; }
    public void setEmail(String email) { Email = email; }

    public String getAddress() { return Address; }
    public void setAddress(String address) { Address = address; }

    @Override
    public String toString() {
        return "Client{" +
                "ID=" + ID +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", CNP='" + CNP + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Email='" + Email + '\'' +
                ", Address='" + Address + '\'' +
                '}';
    }
}
