package model;

import javafx.scene.control.Spinner;

public class Product {
    private int ID;
    private String Name;
    private String Brand;
    private double Price;
    private int Quantity;

    public Product() {
    }

    public Product (String[] paramList) {
        this.Name = paramList[0];
        this.Brand = paramList[1];
        this.Price = paramList[2].equals("") ? 0 : Double.valueOf(paramList[2]);
        this.Quantity = paramList[3].equals("") ? 0 : Integer.valueOf(paramList[3]);
    }

    public int getID() { return ID; }
    public String getName() { return Name; }
    public String getBrand() { return Brand; }
    public double getPrice() { return Price; }
    public int getQuantity() { return Quantity; }

    public void setID(int ID) { this.ID = ID; }
    public void setName(String name) { Name = name; }
    public void setBrand(String brand) { Brand = brand; }
    public void setPrice(double price) { Price = price; }
    public void setQuantity(int quantity) { Quantity = quantity; }

    @Override
    public String toString() {
        return "Product{" +
                "ID=" + ID +
                ", Name='" + Name + '\'' +
                ", Brand='" + Brand + '\'' +
                ", Price=" + Price +
                ", Quantity=" + Quantity +
                '}';
    }
}
