package model;

public class Orders {
    private int ID;
    private int Client_ID;
    private int Product_ID;
    private int Quantity;
    private double Price;

    public Orders() {
    }


    public int getID() { return ID; }
    public int getClient_ID() { return Client_ID; }
    public int getProduct_ID() { return Product_ID; }
    public int getQuantity() { return Quantity; }
    public double getPrice() { return Price; }

    public void setID(int ID) { this.ID = ID; }
    public void setClient_ID(int client_ID) { Client_ID = client_ID; }
    public void setProduct_ID(int product_ID) { Product_ID = product_ID; }
    public void setQuantity(int quantity) { Quantity = quantity; }
    public void setPrice(double price) { Price = price; }

    @Override
    public String toString() {
        return "Order{" +
                "ID=" + ID +
                ", Client_ID=" + Client_ID +
                ", Product_ID=" + Product_ID +
                ", Quantity=" + Quantity +
                ", Price=" + Price +
                '}';
    }
}
