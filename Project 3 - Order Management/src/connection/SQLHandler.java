package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLHandler {
    private static final String URL = "jdbc:mysql://localhost:3306/pt_tema3";
    private static final String USER = "PT";
    private static final String PASS = "PT2019";

    private Connection connection = null;

    private static SQLHandler singleInstance = new SQLHandler();

    private SQLHandler() {}

    private void connect() {
        connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASS);
            System.out.println("Connection created successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        try {
            if(singleInstance.connection == null || singleInstance.connection.isClosed()) {
                singleInstance.connect();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return singleInstance.connection;
    }

    private void close() {
        try {
            connection.close();
            System.out.println("Database connection severed.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void severConnection() {
        if(singleInstance.connection != null) {
            singleInstance.close();
        }
    }
}
