package data_access;

import connection.SQLHandler;
import model.Product;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductEntity extends AbstractEntity<Product> {

    public ArrayList<Product> genericFind(String[] paramList, String orderBy, boolean desc) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM Product WHERE ";
        Field[] fields = Product.class.getDeclaredFields();
        for (int i = 1; i < fields.length; i++) {//start from one because we don't need the ID
            if (paramList[i - 1] != null && !paramList[i - 1].equals("")) {
                query += fields[i].getName() + " = ? AND ";
            }
        }
        query = query.substring(0, query.length() - 5);//eliminate last 'AND'
        query += "ORDER BY " + orderBy + (desc ? " DESC " : " ASC ");
        System.out.println("QUERRY IS: " + query);
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            int paramIndex = 1;
            for (String s : paramList) {
                if (s != null && !s.equals("")) {
                    statement.setObject(paramIndex, s);
                    paramIndex++;
                }
            }
            System.out.println(statement);
            resultSet = statement.executeQuery();
            return materialise(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();

        return null;
    }

    public ArrayList<Product> selectAll(String orderBy, boolean desc) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM Product ORDER BY " + orderBy + (desc ? " DESC " : " ASC ");
        System.out.println(query);
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return materialise(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();
        return null;
    }
}
