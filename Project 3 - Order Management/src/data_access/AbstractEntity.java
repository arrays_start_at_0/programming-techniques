package data_access;

import connection.SQLHandler;
import model.Client;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AbstractEntity<T> {
    private final Class<T> entity;

    AbstractEntity() {
        this.entity = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM " + entity.getSimpleName() + " WHERE ID = ?";
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            ArrayList<T> ret = materialise(resultSet);
            if(!ret.isEmpty()) {
                return ret.get(0);
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();
        return null;
    }

    public ArrayList<T> genericFind(String[] paramList) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM " + entity.getSimpleName() + " WHERE ";
        Field[] fields = entity.getDeclaredFields();
        for (int i = 1; i < fields.length; i++) {//start from one because we don't need the ID
            if (paramList[i - 1] != null && !paramList[i - 1].equals("")) {
                query += fields[i].getName() + " = ? AND ";
            }
        }
        query = query.substring(0, query.length() - 5);//eliminate last 'AND'
        System.out.println("QUERRY IS: " + query);
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            int paramIndex = 1;
            for (String s : paramList) {
                if (s != null && !s.equals("")) {
                    statement.setObject(paramIndex, s);
                    paramIndex++;
                }
            }
            System.out.println(statement);
            resultSet = statement.executeQuery();
            return materialise(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();

        return null;
    }

    public ArrayList<T> selectAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = "SELECT * FROM " + entity.getSimpleName();
        System.out.println(query);
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return materialise(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();
        return null;
    }

    public void genericInsert(String[] paramList) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = "INSERT INTO " + entity.getSimpleName() + " (";
        Field[] fields = entity.getDeclaredFields();
        for (int i = 1; i < fields.length; i++) {
            query += fields[i].getName() + ", ";
        }
        query = query.substring(0, query.length() - 2);
        query += ") VALUES (";
        for (int i = 1; i < fields.length; i++) {//start from one because we don't need the ID
            query += "?, ";
        }
        query = query.substring(0, query.length() - 2);
        query += ")";
        System.out.println("QUERRY IS: " + query);
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            System.out.println(paramList.length);
            for (int i = 0; i < paramList.length; i++) {
                if (paramList[i] != null && !paramList[i].equals("")) {
                    statement.setObject(i + 1, paramList[i]);
                } else {
                    statement.setObject(i + 1, "null");
                }
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();
    }

    public void deleteById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = "DELETE FROM " + entity.getSimpleName() + " WHERE ID = ?";
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();
    }

    public int genericDelete(String[] paramList) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int deleteCount = 0;
        String query = "DELETE FROM " + entity.getSimpleName() + " WHERE ";
        Field[] fields = entity.getDeclaredFields();
        for (int i = 1; i < fields.length; i++) {
            if (paramList[i - 1] != null && !paramList[i - 1].equals("")) {
                query += fields[i].getName() + " = ? AND ";
            }
        }
        query = query.substring(0, query.length() - 5);//eliminate last 'AND'
        System.out.println("QUERRY IS: " + query);
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement("SELECT COUNT(*) FROM " + entity.getSimpleName());
            resultSet = statement.executeQuery();
            resultSet.next();
            deleteCount = resultSet.getInt("COUNT(*)");

            statement = connection.prepareStatement(query);
            int paramIndex = 1;
            for (String s : paramList) {
                if (s != null && !s.equals("")) {
                    statement.setObject(paramIndex, s);
                    paramIndex++;
                }
            }
            statement.execute();

            statement = connection.prepareStatement("SELECT COUNT(*) FROM " + entity.getSimpleName());
            resultSet = statement.executeQuery();
            resultSet.next();
            deleteCount -= resultSet.getInt("COUNT(*)");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();
        return deleteCount;
    }

    public void genericUpdate(String[] paramList, int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = "UPDATE " + entity.getSimpleName() + " SET ";
        Field[] fields = entity.getDeclaredFields();
        for (int i = 1; i < fields.length; i++) {
            if(paramList[i-1] != null && !paramList[i-1].equals("")) {
                query += fields[i].getName() + " = ?, ";
            }
        }
        query = query.substring(0, query.length() - 2);

        query += " WHERE ID = ?";
        System.out.println("QUERRY IS: " + query);
        try {
            connection = SQLHandler.getConnection();
            statement = connection.prepareStatement(query);
            int paramIndex = 1;
            for (String s : paramList) {
                if (s != null && !s.equals("")) {
                    statement.setObject(paramIndex, s);
                    paramIndex++;
                }
            }
            statement.setInt(paramIndex, id);
            System.out.println(statement);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        SQLHandler.severConnection();
    }

    ArrayList<T> materialise(ResultSet res) throws SQLException {
        ArrayList<T> list = new ArrayList<>();
        try {
            while (res.next()) {
                T element = entity.newInstance();
                for (Field f : entity.getDeclaredFields()) {
                    Object value = res.getObject(f.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(f.getName(), entity);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(element, value);
                }
                list.add(element);
            }
        } catch (SQLException | IllegalAccessException | InstantiationException | IntrospectionException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return list;
    }


}
