package business_logic;

import business_logic.validators.*;
import data_access.ClientEntity;
import data_access.ProductEntity;
import model.Product;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class ProductLogic {
    private ProductEntity productEntity;
    private ArrayList<Validator<Product>> validators;

    public ProductLogic() {
        validators = new ArrayList<>();
        validators.add(new PriceValidator());
        validators.add(new QuantityValidator());

        productEntity = new ProductEntity();
    }

    public Product findById(int id) {
        return productEntity.findById(id);
    }

    public ArrayList<Product> find(String[] paramList, String orderBy, boolean desc) {
        return productEntity.genericFind(paramList, orderBy, desc);
    }

    public void insert(String[] paramList) {
        //input validation
        Product P = new Product(paramList);
        for (Validator<Product> V : validators) {
            V.validate(P);
        }
        productEntity.genericInsert(paramList);
    }

    public void deleteById(int id) {
        productEntity.deleteById(id);
    }

    public int delete(String[] paramList) {
        return productEntity.genericDelete(paramList);
    }

    public void update(String[] paramList, int id) {
        //input validation
        Product P = new Product(paramList);
        for (Validator<Product> V : validators) {
            V.validate(P);
        }
        productEntity.genericUpdate(paramList, id);
    }

    public ArrayList<Product> listAll (String orderBy, boolean desc) {
        return productEntity.selectAll(orderBy, desc);
    }
}
