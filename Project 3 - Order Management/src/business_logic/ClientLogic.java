package business_logic;

import business_logic.validators.CNPValidator;
import business_logic.validators.EmailValidator;
import business_logic.validators.PhoneValidator;
import business_logic.validators.Validator;
import data_access.ClientEntity;
import model.Client;

import java.util.ArrayList;

public class ClientLogic {
    private ClientEntity clientEntity;
    private ArrayList<Validator<Client>> validators;

    public ClientLogic() {
        validators = new ArrayList<>();
        validators.add(new EmailValidator());
        validators.add(new CNPValidator());
        validators.add(new PhoneValidator());

        clientEntity = new ClientEntity();
    }

    public Client findById(int id) {
        return clientEntity.findById(id);
    }

    public ArrayList<Client> find(String[] paramList) {
        return clientEntity.genericFind(paramList);
    }

    public void insert(String[] paramList) {
        //input validation
        Client C = new Client(paramList);
        for (Validator<Client> V : validators) {
            V.validate(C);
        }
        clientEntity.genericInsert(paramList);

    }

    public void deleteById(int id) {
        clientEntity.deleteById(id);
    }

    public int delete(String[] paramList) {
        return clientEntity.genericDelete(paramList);
    }

    public void update(String[] paramList, int id) {
        //input validation
        Client C = new Client(paramList);
        for (Validator<Client> V : validators) {
            V.validate(C);
        }
        clientEntity.genericUpdate(paramList, id);
    }

    public ArrayList<Client> listAll() {
        return clientEntity.selectAll();
    }
}
