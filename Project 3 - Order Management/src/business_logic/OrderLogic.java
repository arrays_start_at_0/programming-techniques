package business_logic;

import data_access.OrderEntity;
import model.Client;
import model.Orders;
import model.Product;

import java.util.ArrayList;

public class OrderLogic {
    private OrderEntity orderEntity;

    public OrderLogic() {
        orderEntity = new OrderEntity();
    }

    public ArrayList<Orders> listAll() {
        return orderEntity.selectAll();
    }

    public void deleteById(int id) {
        orderEntity.deleteById(id);
    }

    public void insert(int C_id, int P_id, int quantity, double price) {
        String[] param = new String[4];
        param[0] = String.valueOf(C_id);
        param[1] = String.valueOf(P_id);
        param[2] = String.valueOf(quantity);
        param[3] = String.valueOf(price);
        orderEntity.genericInsert(param);
    }

}
