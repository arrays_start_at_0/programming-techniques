package business_logic.validators;

import model.Client;

import java.util.regex.Pattern;

public class PhoneValidator implements Validator<Client> {

    private static final String PHONE_PATTERN = "(407)([0-9]){8}$";

    @Override
    public void validate(Client client) {
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        if (!client.getPhone().isEmpty() && !pattern.matcher(client.getPhone()).matches()) {
            throw new IllegalArgumentException("Client phone number is invalid!");
        }
    }
}
