package business_logic.validators;

import model.Client;

import java.util.regex.Pattern;

public class EmailValidator implements Validator<Client>{
    private static final String EMAIL_PATTERN = "[\\w-.]+([@])+[a-zA-Z]+([.])+[a-zA-z]{2,}";

    @Override
    public void validate(Client client) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        if (!client.getEmail().isEmpty() && !pattern.matcher(client.getEmail()).matches()) {
            throw new IllegalArgumentException("Client email is invalid!");
        }
    }
}
