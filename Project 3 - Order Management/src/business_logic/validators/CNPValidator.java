package business_logic.validators;

import model.Client;

import java.util.regex.Pattern;

public class CNPValidator implements Validator<Client>{
    private static final String CNP_PATTERN = "([0-9]){13}";

    @Override
    public void validate(Client client) {
        Pattern pattern = Pattern.compile(CNP_PATTERN);
        if (!client.getCNP().isEmpty() && !pattern.matcher(client.getCNP()).matches()) {
            throw new IllegalArgumentException("Client CNP is invalid!");
        }
    }
}
