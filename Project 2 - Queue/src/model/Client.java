package model;

import java.util.Random;

public class Client {
    private int serviceTime;
    private int waitingTime;
    private int arrivalTime;
    int totalWaitingTime = 0;
    private String name;
    private int ID;
    private static int nrClients = 0;
    final private String[] clientNames = {"Ana", "Liviu", "Andrei", "Cezara", "Fane", "Alexei", "Cristi", "Grig", "Rares", "Mădă", "Iulia", "Maria", "Marcel", "Giosan", "Felix"};

    public Client(int minST, int maxST, int minAT, int maxAT) {
        Random rnd = new Random();
        serviceTime = rnd.nextInt(maxST) + minST;
        waitingTime = serviceTime;
        arrivalTime = rnd.nextInt(maxAT) + minAT;
        name = clientNames[rnd.nextInt(clientNames.length)];
        nrClients++;
        ID = nrClients;
    }

    boolean decreaseTime() {
        waitingTime--;
        return waitingTime >= 0;
    }

    double waitRate() { return (double)(serviceTime - waitingTime) / (double)totalWaitingTime; }
    public double getProgress() { return 1 - (double) waitingTime / serviceTime; }

    public int getArrivalTime() { return arrivalTime; }

    public String toString() { return name + "-" + ID; }
}
