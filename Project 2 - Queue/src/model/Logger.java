package model;

import controller.Server;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Logger {
    private  ArrayList<String> record = new ArrayList<>();
    private  TextArea textLog = new TextArea();

    public Logger () {
    }

    public void linkUI (TextArea t) {
        textLog = t;
        textLog.setText("SIMULATION START\n");
    }

    public synchronized void add(String s, int time) {
        record.add((time > 720 ? "OVERTIME" : "") + ">[T:" + time + "] " + s);
        Platform.runLater(() -> textLog.appendText((time > 720 ? "OVERTIME" : "") + ">[T:" + time + "] " + s + '\n'));
    }

    public void saveLog(Server server) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("[yyyy-MM-dd][HH-mm-ss]");
            String fileName = "SIM_" + (dateFormat.format(new Date())) + ".txt";
            FileOutputStream fos = new FileOutputStream(fileName);

            fos.write("Simulation parameters: ".getBytes());
            fos.write(( "\n\tNumber of queues: " + server.getnrQueues() +
                        "\n\tNumber of clients: " + server.getProcessed() +
                        "\n\tQueue capacity: " + server.getQueueCap() +
                        "\n\tMinimum service time: " + server.getMinST() +
                        "\n\tMaximum service time: " + server.getMaxST() +
                        "\n\tMinimum arrival time: " + server.getMinAT() +
                        "\n\tMaximum arrival time: " + server.getMaxAT() +
                        "\n-------------------------------------------------------\n\n").getBytes());
            fos.write( "Simulation results: ".getBytes());
            fos.write(( "\n\tAverage speed: " + server.calculateAvgSpeed() +
                        "\n\tAverage waiting time: " + server.calculateAvgWait() +
                        "\n\tPeak time: " + server.getPeakTime() +
                        "\n-------------------------------------------------------\n\n").getBytes());
            fos.write( "Queue information: ".getBytes());
            for(Queue Q: server.getQueues()) {
                fos.write(( "\n\tQueue " + Q.getQueueNr() + ":" +
                            "\n\t\tClients processed: " + Q.getProcessed() +
                            "\n\t\tSpeed: " + Q.getSpeed() +
                            "\n\t\tAverage waiting time: " + Q.getAvgWait() +
                            "\n\t\tTime spent empty: " + Q.getEmptyTime()).getBytes());
            }
            fos.write("\n-------------------------------------------------------\n\n".getBytes());
            fos.write("LOGGED INFORMATION:".getBytes());
            for(String S: record) {
                fos.write(("\n" + S).getBytes());
            }
        } catch (Exception e) {
            System.out.println("This should never print");
        }

    }

}
