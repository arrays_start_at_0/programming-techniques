package controller;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import model.Client;
import model.ClientComparator;
import model.Logger;
import model.Queue;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class Server implements Runnable {
    ///simmulation settings
    private int minServiceTime;
    private int maxServiceTime;
    private int minArrivalTime;
    private int maxArrivalTime;
    private int maxQueueCapacity;
    //---------------------------------------------------------------------
    private ProgressBar bar;
    private Text timeText;
    private Text statusText;
    private ArrayList<VBox> visualQueue;

    private int counter;
    private double speed;//1-10
    private int peakTime;
    private int maxIdle = -1;
    private int clientsInSystem;
    private int clientsProcessed;

    private boolean closeThread = false;

    private ArrayList<Queue> queueList;
    private PriorityQueue<Client> expectedClients;
    private PriorityQueue<Client> postponedClients;
    private ArrayList<Client> toInsert;

    private Logger log;

    public Server(int nrQueues, int nrClients, double simSpeed, int queCapacity, int minService, int maxService, int minArrival, int maxArrival) {
        counter = 0;
        speed = simSpeed;

        maxQueueCapacity = queCapacity;
        minServiceTime = minService;
        maxServiceTime = maxService;
        minArrivalTime = minArrival;
        maxArrivalTime = maxArrival;

        queueList = new ArrayList<>();
        expectedClients = new PriorityQueue<>(new ClientComparator());
        postponedClients = new PriorityQueue<>(new ClientComparator());
        toInsert = new ArrayList<>();
        log = new Logger();
        populateShop(nrClients, nrQueues);

    }

    private void populateShop(int nrClients, int nrQueues) {
        for (int i = 0; i < nrClients; i++) {
            expectedClients.add(new Client(minServiceTime, maxServiceTime, minArrivalTime, maxArrivalTime));
        }
        for (int i = 0; i < nrQueues; i++) {
            queueList.add(new Queue(i, counter, speed, maxQueueCapacity, log));
        }
    }

    private void startQueues() {
        for (Queue Q : queueList) {
            Thread t = new Thread(Q);
            t.start();
        }
    }

    public void linkUI(ProgressBar b, Text time, Text status, ArrayList<VBox> queues, TextArea logArea) {
        bar = b;
        timeText = time;
        statusText = status;
        visualQueue = queues;
        log.linkUI(logArea);
    }

    public void run() {
        while (!closeThread) {
            if (counter == 0) {
                startQueues();
            }

            addPostponed();
            toInsert = getNewClients(counter);
            if (!toInsert.isEmpty()) {
                addToQueue(toInsert);
            }

            counter++;

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    bar.setProgress((double) counter / 720);
                    if (counter > 720) {
                        bar.setStyle("-fx-accent: red");
                    }

                    clientsInSystem = 0;
                    Server.this.makeTime();
                    Server.this.makeStatus();
                    Server.this.updateQueues();
                    clientsInSystem += postponedClients.size();

                    if (maxIdle < clientsInSystem) {
                        maxIdle = clientsInSystem;
                        peakTime = counter;
                    }
                }
            });

            try {
                Thread.sleep((long) ((11 - speed) * 100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<Client> getNewClients(int currentTime) {//needs to return more than one and in O(1)
        ArrayList<Client> retClient = new ArrayList<>();
        while (!expectedClients.isEmpty() && expectedClients.peek().getArrivalTime() == currentTime) {
            retClient.add(expectedClients.poll());
        }
        return retClient;
    }

    private void addToQueue(ArrayList<Client> toInsert) {//needs to happen in O(1) to avoid desync
        Queue Q = getSmallestQueue();
        while (!toInsert.isEmpty() && Q != null) {
            Client C = toInsert.get(0);
            toInsert.remove(0);
            //System.out.println("Client " + C + " has been INSERTED to queue " + Q.getQueueNr() + ".");
            log.add("Client " + C + " has been INSERTED to queue " + Q.getQueueNr() + ".", counter);
            Q.addClient(C);

            final int nr = Q.getQueueNr();
            Platform.runLater(() -> {
                ProgressIndicator P = new ProgressIndicator(C.getProgress());
                bindTooltip(P, new Tooltip(C.toString() + "\nQueue " + nr));
                visualQueue.get(nr).getChildren().add(P);
            });

            Q = getSmallestQueue();
        }
        while (!toInsert.isEmpty()) {
            Client C = toInsert.get(0);
            toInsert.remove(0);
            //System.out.println("Client " + C + " has been POSTPONED");
            log.add("Client " + C + " has been POSTPONED", counter);
            postponedClients.add(C);
        }
    }

    private void addPostponed() {
        Queue Q = getSmallestQueue();
        while (!postponedClients.isEmpty() && Q != null) {
            Client C = postponedClients.poll();
            //System.out.println("Client " + C + " has been POST-INSERTED to queue " + Q.getQueueNr() + ".");
            log.add("Client " + C + " has been POST-INSERTED to queue " + Q.getQueueNr() + ".", counter);
            Q.addClient(C);

            final int nr = Q.getQueueNr();
            Platform.runLater(() -> {
                ProgressIndicator P = new ProgressIndicator(C.getProgress());
                bindTooltip(P, new Tooltip(C.toString() + "\nQueue " + nr));
                visualQueue.get(nr).getChildren().add(P);
            });

            Q = getSmallestQueue();
        }
    }

    private Queue getSmallestQueue() {
        Queue chosenQ = null;
        int max = 0;
        for (Queue Q : queueList) {
            if (max < Q.emptySlots()) {
                max = Q.emptySlots();
                chosenQ = Q;
            }
        }
        return chosenQ;
    }

    private void makeTime() {
        String H = String.valueOf(counter / 60 + 8);
        String M = String.valueOf(counter % 60);
        if (H.length() == 1) {
            H = "0" + H;
        }
        if (M.length() == 1) {
            M = "0" + M;
        }
        timeText.setText(H + ":" + M);
    }

    private void makeStatus() {
        String s = "";
        s += "Clients yet to arrive: ";
        s += (expectedClients.size()) + "\n";

        if (!expectedClients.isEmpty()) {
            s += "Next client in: " + (expectedClients.peek().getArrivalTime() - counter) + "\n";
        }

        s += "Clients not fitted into any queue: ";
        s += postponedClients.size() + "\n";

        int totalProcessed = 0;
        String qString = "";
        double maxSpeed = 0;
        int fastQ = -1;
        boolean closeQueues = true;
        for (Queue Q : queueList) {
            clientsInSystem += Q.getNrClients();
            if (!Q.isEmpty() || expectedClients.size() != 0 || postponedClients.size() != 0) {
                closeQueues = false;
            }
            totalProcessed += Q.getProcessed();
            double speed = Q.getSpeed();
            if (maxSpeed < speed) {
                fastQ = Q.getQueueNr();
                maxSpeed = speed;
            }
            if (Q.getSpeed() > 0) {
                qString += "Q" + Q.getQueueNr() + ": " + speed + " People: " + Q.getNrClients() + "\n";
            }
        }

        closeThread = closeQueues;
        if(closeThread) { close(); }

        s += "Clients processed: " + totalProcessed + "\n";
        if (fastQ != -1) {
            s += "Fastest queue: Queue " + fastQ + " [" + maxSpeed + "]\n";
        }
        s += "\n";
        s += qString;

        statusText.setText(s);
    }

    private void updateQueues() {
        for(VBox V : visualQueue) {
            if(!V.getChildren().isEmpty()) {
                ProgressIndicator P = (ProgressIndicator) V.getChildren().get(0);
                if (P.getProgress() == 1) {
                    V.getChildren().remove(0);
                }
            }
        }
        for(Queue Q : queueList) {
            if(!visualQueue.get(Q.getQueueNr()).getChildren().isEmpty() && !Q.isEmpty()) {
                ProgressIndicator P = (ProgressIndicator) visualQueue.get(Q.getQueueNr()).getChildren().get(0);
                if(counter > 720) {
                    P.setStyle(" -fx-progress-color: red;");
                }
                Client C = Q.getClientList().peek();
                if(C.getProgress() < P.getProgress()) {//last client has been eliminated from queue
                    visualQueue.get(Q.getQueueNr()).getChildren().remove(0);
                } else  {
                    P.setProgress(C.getProgress());
                }
            }
        }
    }

    public void close() {
        closeThread = true;
        log.add("END OF SIMULATION", counter);
        clientsProcessed = 0;
        for (Queue Q : queueList) {
            clientsProcessed += Q.getProcessed();
            Q.close();
        }
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("SIMULATION HAS ENDED");
            alert.setHeaderText("Simulation has ended in " + counter +" steps." +
                                "\nIt has proccessed " + clientsProcessed + " clients, " +
                                "\nwith an average waiting time of " + calculateAvgWait() +
                                "\nand an average speed of " + calculateAvgSpeed() + "." +
                                "\nPeak time was around " + peakTime + ".");
            alert.show();
        });
    }

    public void saveLog() {
        log.saveLog(this);
    }

    public boolean isRunning() {
        return !closeThread;
    }

    public double calculateAvgWait() {
        double avgWait = 0;
        for(Queue Q: queueList) {
            avgWait += Q.getAvgWait();
        }
        return avgWait / getnrQueues();
    }

    public double calculateAvgSpeed() {
        double avgSpeed = 0;
        for(Queue Q: queueList) {
            avgSpeed += Q.getSpeed();
        }
        return avgSpeed / getnrQueues();
    }

    public ArrayList<Queue> getQueues() {
        return queueList;
    }
    public double getSpeed() { return speed; }
    public int getTime() { return counter; }
    public int getPeakTime() {
        return peakTime;
    }

    public int getProcessed() { return clientsProcessed; }
    public int getnrQueues() {
        return queueList.size();
    }
    public int getQueueCap() { return maxQueueCapacity; }
    public int getMinST() { return minServiceTime; }
    public int getMaxST() { return maxServiceTime; }
    public int getMinAT() { return minArrivalTime; }
    public int getMaxAT() { return maxArrivalTime; }

    public String toString() {
        String s = "Current time: " + counter + "\n";
        for (int i = 0; i < queueList.size(); i++) {
            s += "Queue " + (i + 1) + ": " + queueList.get(i) + "\n";
        }
        s += "--------------------------------------------------\n";
        return s;
    }

    //function eliminates tooltip default delay, making the UI more responsive
    //Author StackOverflow user Aleks Messier
    private static void bindTooltip(final Node node, final Tooltip tooltip) {
        node.setOnMouseMoved(event -> {
            // +15 moves the tooltip 15 pixels below the mouse cursor;
            // if you don't change the y coordinate of the tooltip, you
            // will see constant screen flicker
            tooltip.show(node, event.getScreenX(), event.getScreenY() + 15);
        });
        node.setOnMouseExited(event -> tooltip.hide());
    }
}
